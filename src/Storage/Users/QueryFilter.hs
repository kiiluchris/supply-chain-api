{-# LANGUAGE DeriveGeneric #-}

module Storage.Users.QueryFilter where

import           Data.Aeson               (FromJSON (..), eitherDecode,
                                           genericParseJSON)
import           Data.ByteString.Lazy     as LBS (fromStrict)
import           Data.Text                (Text, pack)
import           Data.Text.Encoding       (encodeUtf8)
import           GHC.Generics             (Generic)
import           Servant                  (FromHttpApiData (..))

import           Models                   (ProjectId, UserId)
import           Storage.Filters.Defaults
import           Utils.Json               (genericJsonOptionsKebab)

data UserFilter =
  UserFilter
    { userFilterInName     :: Maybe Text
    , userFilterInUserName :: Maybe Text
    }
  deriving (Generic, Show)

instance FromJSON UserFilter where
  parseJSON = genericParseJSON (genericJsonOptionsKebab "userFilter" '-')

instance DefaultQueryFilter UserFilter where
  defaultFilter = UserFilter Nothing Nothing

instance FromHttpApiData UserFilter where
  parseQueryParam x =
    case eitherDecode $ LBS.fromStrict $ encodeUtf8 x of
      Left err  -> Left $ pack err
      Right val -> Right val
