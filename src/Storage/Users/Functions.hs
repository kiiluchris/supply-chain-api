{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}

module Storage.Users.Functions where

import           Control.Monad.IO.Class   (MonadIO)
import           Control.Monad.Reader     (MonadReader)
import           Database.Esqueleto
import           Database.Persist.Sql     (ConnectionPool, DeleteCascade,
                                           Entity, entityVal, insertEntity,
                                           insertUniqueEntity)

import           App.Contracts            (Has, grab)
import           Effects.Db               (runWithPool)
import           Models                   (EntityField (UserName, UserUserName),
                                           User)
import           Storage.Filters          (QueryFilter (..), UserFilter (..))
import           Storage.Types            (MaybeKeyModelPair, StorageTypes (..))
import           Storage.Utils.PersistSql

saveUser ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> User
  -> m (MaybeKeyModelPair User)
saveUser SqlStorage user = do
  pool <- grab @ConnectionPool
  createdUser <- runWithPool pool $ insertUniqueEntity user
  return $ maybeEntityToKeyRecordPair createdUser

queryUsers ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Maybe (QueryFilter UserFilter)
  -> m [User]
queryUsers SqlStorage queryParams = do
  pool <- grab @ConnectionPool
  user <-
    runWithPool pool $
    select . from $ \r -> do
      maybeQuery queryParams $ \(QueryFilter query page pageSize) -> do
        maybeQuery query $ \(UserFilter inName inUserName) -> do
          maybeIn r UserName inName
          maybeIn r UserUserName inUserName
        paginateQuery (page, pageSize)
      return r
  return $ entityVal <$> user

retrieveUser ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key User
  -> m (MaybeKeyModelPair User)
retrieveUser SqlStorage rid = do
  pool <- grab @ConnectionPool
  user <- runWithPool pool $ getEntity rid
  return $ maybeEntityToKeyRecordPair user

updateUser ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key User
  -> User
  -> m ()
updateUser SqlStorage rid user = do
  pool <- grab @ConnectionPool
  runWithPool pool $ replace rid user

deleteUser ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key User
  -> m ()
deleteUser SqlStorage rid = do
  pool <- grab @ConnectionPool
  runWithPool pool $ deleteKey rid
