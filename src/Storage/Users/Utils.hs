module Storage.Users.Utils where

import           Data.Text (Text)
import           Models    (User (..))

compareDetail :: (Eq a) => (record -> a) -> record -> record -> Bool
compareDetail propGetter user1 user2 = propGetter user1 == propGetter user2

compareAccountDetails :: User -> User -> Text
compareAccountDetails existingUser otherUser
  | compareDetail userPhoneNumber existingUser otherUser =
    "Phone number already used"
  | compareDetail userName existingUser otherUser = "Username already used"
  | otherwise = "Account already exists"
