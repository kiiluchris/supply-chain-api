module Storage.Utils.PersistSql where

import           Database.Esqueleto (Entity, EntityField, Key, PersistEntity,
                                     PersistField, SqlExpr, SqlQuery, SqlString,
                                     entityKey, entityVal, like, limit, offset,
                                     val, where_, (%), (++.), (==.), (^.))

maybeWhere ::
     (PersistEntity a, PersistField b)
  => SqlExpr (Entity a)
  -> EntityField a b
  -> Maybe b
  -> SqlQuery ()
maybeWhere entity field (Just value) = where_ $ entity ^. field ==. val value
maybeWhere entity field Nothing      = return ()

maybeIn ::
     (PersistEntity a, PersistField b, SqlString b)
  => SqlExpr (Entity a)
  -> EntityField a b
  -> Maybe b
  -> SqlQuery ()
maybeIn entity field (Just value) =
  where_ $ like (entity ^. field) ((%) ++. val value ++. (%))
maybeIn entity field Nothing = return ()

maybeQuery :: Maybe a -> (a -> SqlQuery ()) -> SqlQuery ()
maybeQuery (Just value) query = query value
maybeQuery Nothing _          = return ()

paginateQuery :: (Maybe Int, Maybe Int) -> SqlQuery ()
paginateQuery (Just page, Just pageSize)
  | page < 2 = limit (fromIntegral pageSize)
  | otherwise =
    limit (fromIntegral pageSize) >>
    (offset . fromIntegral . (pageSize *) . subtract 1) page
paginateQuery (Nothing, Just pageSize) = limit (fromIntegral pageSize)
paginateQuery _ = return ()

maybeEntityToKeyRecordPair :: Maybe (Entity a) -> Maybe (Key a, a)
maybeEntityToKeyRecordPair = fmap entityToKeyRecordPair

entityToKeyRecordPair :: Entity a -> (Key a, a)
entityToKeyRecordPair entity = (entityKey entity, entityVal entity)
