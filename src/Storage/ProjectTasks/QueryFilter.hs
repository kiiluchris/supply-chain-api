{-# LANGUAGE DeriveGeneric #-}

module Storage.ProjectTasks.QueryFilter where

import           Data.Aeson               (FromJSON (..), eitherDecode,
                                           genericParseJSON)
import           Data.ByteString.Lazy     as LBS (fromStrict)
import           Data.Text                (Text, pack)
import           Data.Text.Encoding       (encodeUtf8)
import           GHC.Generics             (Generic)
import           Servant                  (FromHttpApiData (..))

import           Models                   (ContractorProfileId, ProjectId,
                                           UserId)
import           Storage.Filters.Defaults
import           Utils.Json               (genericJsonOptionsKebab)

data ProjectTaskFilter =
  ProjectTaskFilter
    { projectTaskFilterOwner      :: Maybe UserId
    , projectTaskFilterContractor :: Maybe ContractorProfileId
    , projectTaskFilterProject    :: Maybe ProjectId
    , projectTaskFilterInTitle    :: Maybe Text
    , projectTaskFilterIsComplete :: Maybe Bool
    }
  deriving (Generic, Show)

instance FromJSON ProjectTaskFilter where
  parseJSON = genericParseJSON (genericJsonOptionsKebab "projectTaskFilter" '-')

instance DefaultQueryFilter ProjectTaskFilter where
  defaultFilter = ProjectTaskFilter Nothing Nothing Nothing Nothing Nothing

instance FromHttpApiData ProjectTaskFilter where
  parseQueryParam x =
    case eitherDecode $ LBS.fromStrict $ encodeUtf8 x of
      Left err  -> Left $ pack err
      Right val -> Right val
