{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}

module Storage.ProjectTasks.Functions where

import           Control.Monad.IO.Class   (MonadIO)
import           Control.Monad.Reader     (MonadReader)
import           Database.Esqueleto
import           Database.Persist.Sql     (ConnectionPool, DeleteCascade,
                                           Entity, entityVal, insertEntity,
                                           insertUniqueEntity)

import           App.Contracts            (Has, grab)
import           Effects.Db               (runWithPool)
import           Models                   (EntityField (ProjectTaskContractor, ProjectTaskIsComplete, ProjectTaskOwner, ProjectTaskProject, ProjectTaskTitle),
                                           ProjectTask)
import           Storage.Filters          (ProjectTaskFilter (..),
                                           QueryFilter (..))
import           Storage.Types            (KeyModelPair, MaybeKeyModelPair,
                                           StorageTypes (..))
import           Storage.Utils.PersistSql

saveProjectTask ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> ProjectTask
  -> m (KeyModelPair ProjectTask)
saveProjectTask SqlStorage projectTask = do
  pool <- grab @ConnectionPool
  createdProjectTask <- runWithPool pool $ insertEntity projectTask
  return $ entityToKeyRecordPair createdProjectTask

queryProjectTasks ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Maybe (QueryFilter ProjectTaskFilter)
  -> m [ProjectTask]
queryProjectTasks SqlStorage queryParams = do
  pool <- grab @ConnectionPool
  projectTask <-
    runWithPool pool $
    select . from $ \r -> do
      maybeQuery queryParams $ \(QueryFilter query page pageSize) -> do
        maybeQuery query $ \(ProjectTaskFilter owner contractor project title isComplete) -> do
          maybeWhere r ProjectTaskOwner owner
          maybeWhere r ProjectTaskContractor contractor
          maybeWhere r ProjectTaskProject project
          maybeIn r ProjectTaskTitle title
          maybeWhere r ProjectTaskIsComplete isComplete
        paginateQuery (page, pageSize)
      return r
  return $ entityVal <$> projectTask

retrieveProjectTask ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key ProjectTask
  -> m (MaybeKeyModelPair ProjectTask)
retrieveProjectTask SqlStorage rid = do
  pool <- grab @ConnectionPool
  projectTask <- runWithPool pool $ getEntity rid
  return $ maybeEntityToKeyRecordPair projectTask

updateProjectTask ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key ProjectTask
  -> ProjectTask
  -> m ()
updateProjectTask SqlStorage rid projectTask = do
  pool <- grab @ConnectionPool
  runWithPool pool $ replace rid projectTask

deleteProjectTask ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key ProjectTask
  -> m ()
deleteProjectTask SqlStorage rid = do
  pool <- grab @ConnectionPool
  runWithPool pool $ deleteKey rid
