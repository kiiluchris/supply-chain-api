{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE StandaloneDeriving #-}

module Storage.QueryFilter where

import           Data.Aeson               (FromJSON (..), eitherDecode,
                                           genericParseJSON)
import           Data.ByteString.Lazy     as LBS (fromStrict)
import           Data.Text                (Text, pack)
import           Data.Text.Encoding       (encodeUtf8)
import           GHC.Generics             (Generic)
import           Servant                  (FromHttpApiData (..))

import           Storage.Filters.Defaults
import           Utils.Json               (genericJsonOptionsKebab)

data QueryFilter a =
  QueryFilter
    { queryFilterQ        :: Maybe a
    , queryFilterPage     :: Maybe Int
    , queryFilterPageSize :: Maybe Int
    }
  deriving (Generic)

deriving instance Show a => Show (QueryFilter a)

instance (FromJSON a) => FromJSON (QueryFilter a) where
  parseJSON = genericParseJSON (genericJsonOptionsKebab "queryFilter" '-')

instance DefaultQueryFilter a => DefaultQueryFilter (QueryFilter a) where
  defaultFilter = QueryFilter Nothing Nothing Nothing

instance (FromHttpApiData a, FromJSON a) =>
         FromHttpApiData (QueryFilter a) where
  parseQueryParam x =
    case eitherDecode $ LBS.fromStrict $ encodeUtf8 x of
      Left err  -> Left $ pack err
      Right val -> Right val
