{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}

module Storage.ContractorProfiles.Functions where

import           Control.Monad.IO.Class   (MonadIO)
import           Control.Monad.Reader     (MonadReader)
import           Database.Esqueleto
import           Database.Persist.Sql     (ConnectionPool, DeleteCascade,
                                           Entity, entityVal, insertEntity,
                                           insertUniqueEntity)

import           App.Contracts            (Has, grab)
import           Effects.Db               (runWithPool)
import           Models                   (ContractorProfile, EntityField (ContractorProfileCompany))
import           Storage.Filters          (ContractorProfileFilter (..),
                                           QueryFilter (..))
import           Storage.Types            (KeyModelPair, MaybeKeyModelPair,
                                           StorageTypes (..))
import           Storage.Utils.PersistSql

saveContractorProfile ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> ContractorProfile
  -> m (KeyModelPair ContractorProfile)
saveContractorProfile SqlStorage contractorProfile = do
  pool <- grab @ConnectionPool
  createdContractorProfile <- runWithPool pool $ insertEntity contractorProfile
  return $ entityToKeyRecordPair createdContractorProfile

queryContractorProfiles ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Maybe (QueryFilter ContractorProfileFilter)
  -> m [ContractorProfile]
queryContractorProfiles SqlStorage queryParams = do
  pool <- grab @ConnectionPool
  contractorProfile <-
    runWithPool pool $
    select . from $ \r -> do
      maybeQuery queryParams $ \(QueryFilter query page pageSize) -> do
        maybeQuery query $ \(ContractorProfileFilter company) ->
          maybeIn r ContractorProfileCompany company
        paginateQuery (page, pageSize)
      return r
  return $ entityVal <$> contractorProfile

retrieveContractorProfile ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key ContractorProfile
  -> m (MaybeKeyModelPair ContractorProfile)
retrieveContractorProfile SqlStorage rid = do
  pool <- grab @ConnectionPool
  contractorProfile <- runWithPool pool $ getEntity rid
  return $ maybeEntityToKeyRecordPair contractorProfile

updateContractorProfile ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key ContractorProfile
  -> ContractorProfile
  -> m ()
updateContractorProfile SqlStorage rid contractorProfile = do
  pool <- grab @ConnectionPool
  runWithPool pool $ replace rid contractorProfile

deleteContractorProfile ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key ContractorProfile
  -> m ()
deleteContractorProfile SqlStorage rid = do
  pool <- grab @ConnectionPool
  runWithPool pool $ deleteKey rid
