{-# LANGUAGE DeriveGeneric #-}

module Storage.ContractorProfiles.QueryFilter where

import           Data.Aeson               (FromJSON (..), eitherDecode,
                                           genericParseJSON)
import           Data.ByteString.Lazy     as LBS (fromStrict)
import           Data.Text                (Text, pack)
import           Data.Text.Encoding       (encodeUtf8)
import           GHC.Generics             (Generic)
import           Servant                  (FromHttpApiData (..))

import           Models                   (ProjectId, UserId)
import           Storage.Filters.Defaults
import           Utils.Json               (genericJsonOptionsKebab)

data ContractorProfileFilter =
  ContractorProfileFilter
    { contractorProfileFilterInCompany :: Maybe Text
    }
  deriving (Generic, Show)

instance FromJSON ContractorProfileFilter where
  parseJSON =
    genericParseJSON (genericJsonOptionsKebab "contractorProfileFilter" '-')

instance FromHttpApiData ContractorProfileFilter where
  parseQueryParam x =
    case eitherDecode $ LBS.fromStrict $ encodeUtf8 x of
      Left err  -> Left $ pack err
      Right val -> Right val
