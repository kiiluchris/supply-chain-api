{-# LANGUAGE DeriveGeneric #-}

module Storage.Reviews.QueryFilter where

import           Data.Aeson               (FromJSON (..), eitherDecode,
                                           genericParseJSON)
import           Data.ByteString.Lazy     as LBS (fromStrict)
import           Data.Text                (Text, pack)
import           Data.Text.Encoding       (encodeUtf8)
import           Servant                  (FromHttpApiData (..))

import           GHC.Generics             (Generic)

import           Models                   (ProjectId, UserId)
import           Storage.Filters.Defaults
import           Utils.Json               (genericJsonOptionsKebab)

data ReviewFilter =
  ReviewFilter
    { reviewFilterPoster   :: Maybe UserId
    , reviewFilterReviewee :: Maybe UserId
    , reviewFilterProject  :: Maybe ProjectId
    }
  deriving (Generic, Show)

instance FromJSON ReviewFilter where
  parseJSON = genericParseJSON (genericJsonOptionsKebab "reviewFilter" '-')

instance DefaultQueryFilter ReviewFilter where
  defaultFilter = ReviewFilter Nothing Nothing Nothing

instance FromHttpApiData ReviewFilter where
  parseQueryParam x =
    case eitherDecode $ LBS.fromStrict $ encodeUtf8 x of
      Left err  -> Left $ pack err
      Right val -> Right val
