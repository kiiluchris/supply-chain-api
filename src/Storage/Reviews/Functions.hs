{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}

module Storage.Reviews.Functions where

import           Control.Monad.IO.Class   (MonadIO)
import           Control.Monad.Reader     (MonadReader)
import           Database.Esqueleto
import           Database.Persist.Sql     (ConnectionPool, DeleteCascade,
                                           Entity, entityVal, insertEntity,
                                           insertUniqueEntity)

import           App.Contracts            (Has, grab)
import           Effects.Db               (runWithPool)
import           Models                   (EntityField (ReviewPoster, ReviewProject, ReviewReviewee),
                                           Review)
import           Storage.Filters          (QueryFilter (..), ReviewFilter (..))
import           Storage.Types            (KeyModelPair, MaybeKeyModelPair,
                                           StorageTypes (..))
import           Storage.Utils.PersistSql

saveReview ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Review
  -> m (KeyModelPair Review)
saveReview SqlStorage review = do
  pool <- grab @ConnectionPool
  createdReview <- runWithPool pool $ insertEntity review
  return $ entityToKeyRecordPair createdReview

queryReviews ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Maybe (QueryFilter ReviewFilter)
  -> m [Review]
queryReviews SqlStorage queryParams = do
  pool <- grab @ConnectionPool
  review <-
    runWithPool pool $
    select . from $ \r -> do
      maybeQuery queryParams $ \(QueryFilter query page pageSize) -> do
        maybeQuery query $ \(ReviewFilter poster reviewee project) -> do
          maybeWhere r ReviewPoster poster
          maybeWhere r ReviewReviewee reviewee
          maybeWhere r ReviewProject project
        paginateQuery (page, pageSize)
      return r
  return $ entityVal <$> review

retrieveReview ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key Review
  -> m (MaybeKeyModelPair Review)
retrieveReview SqlStorage rid = do
  pool <- grab @ConnectionPool
  review <- runWithPool pool $ getEntity rid
  return $ maybeEntityToKeyRecordPair review

updateReview ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key Review
  -> Review
  -> m ()
updateReview SqlStorage rid review = do
  pool <- grab @ConnectionPool
  runWithPool pool $ replace rid review

deleteReview ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key Review
  -> m ()
deleteReview SqlStorage rid = do
  pool <- grab @ConnectionPool
  runWithPool pool $ deleteKey rid
