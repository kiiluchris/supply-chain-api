{-# LANGUAGE DeriveGeneric #-}

module Storage.Projects.QueryFilter where

import           Data.Aeson               (FromJSON (..), eitherDecode,
                                           genericParseJSON)
import           Data.ByteString.Lazy     as LBS (fromStrict)
import           Data.Text                (Text, pack)
import           Data.Text.Encoding       (encodeUtf8)
import           GHC.Generics             (Generic)
import           Servant                  (FromHttpApiData (..))

import           Models                   (ProjectId, UserId)
import           Storage.Filters.Defaults
import           Utils.Json               (genericJsonOptionsKebab)

data ProjectFilter =
  ProjectFilter
    { projectFilterOwner   :: Maybe UserId
    , projectFilterType    :: Maybe Text
    , projectFilterTitle   :: Maybe Text
    , projectFilterInTitle :: Maybe Text
    }
  deriving (Generic, Show)

instance FromJSON ProjectFilter where
  parseJSON = genericParseJSON (genericJsonOptionsKebab "projectFilter" '-')

instance DefaultQueryFilter ProjectFilter where
  defaultFilter = ProjectFilter Nothing Nothing Nothing Nothing

instance FromHttpApiData ProjectFilter where
  parseQueryParam x =
    case eitherDecode $ LBS.fromStrict $ encodeUtf8 x of
      Left err  -> Left $ pack err
      Right val -> Right val
