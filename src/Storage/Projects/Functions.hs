{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}

module Storage.Projects.Functions where

import           Control.Monad.IO.Class   (MonadIO)
import           Control.Monad.Reader     (MonadReader)
import           Database.Esqueleto
import           Database.Persist.Sql     (ConnectionPool, DeleteCascade,
                                           Entity, entityVal, insertEntity,
                                           insertUniqueEntity)

import           App.Contracts            (Has, grab)
import           Effects.Db               (runWithPool)
import           Models                   (EntityField (ProjectOwner, ProjectTitle, ProjectType),
                                           Project)
import           Storage.Filters          (ProjectFilter (..), QueryFilter (..))
import           Storage.Types            (KeyModelPair, MaybeKeyModelPair,
                                           StorageTypes (..))
import           Storage.Utils.PersistSql

saveProject ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Project
  -> m (KeyModelPair Project)
saveProject SqlStorage project = do
  pool <- grab @ConnectionPool
  createdProject <- runWithPool pool $ insertEntity project
  return $ entityToKeyRecordPair createdProject

queryProjects ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Maybe (QueryFilter ProjectFilter)
  -> m [Project]
queryProjects SqlStorage queryParams = do
  pool <- grab @ConnectionPool
  project <-
    runWithPool pool $
    select . from $ \r -> do
      maybeQuery queryParams $ \(QueryFilter query page pageSize) -> do
        maybeQuery query $ \(ProjectFilter owner type_ title inTitle) -> do
          maybeWhere r ProjectOwner owner
          maybeWhere r ProjectType type_
          maybeIn r ProjectTitle title
          maybeIn r ProjectTitle inTitle
        paginateQuery (page, pageSize)
      return r
  return $ entityVal <$> project

retrieveProject ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key Project
  -> m (MaybeKeyModelPair Project)
retrieveProject SqlStorage rid = do
  pool <- grab @ConnectionPool
  project <- runWithPool pool $ getEntity rid
  return $ maybeEntityToKeyRecordPair project

updateProject ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key Project
  -> Project
  -> m ()
updateProject SqlStorage rid project = do
  pool <- grab @ConnectionPool
  runWithPool pool $ replace rid project

deleteProject ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key Project
  -> m ()
deleteProject SqlStorage rid = do
  pool <- grab @ConnectionPool
  runWithPool pool $ deleteKey rid
