{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}

module Storage.Accounts.Functions where

import           Control.Monad.IO.Class   (MonadIO)
import           Control.Monad.Reader     (MonadReader)
import           Data.Maybe               (listToMaybe)
import           Data.Text                (Text)
import           Database.Esqueleto
import qualified Database.Persist         as P

import           App.Contracts            (Has, grab)
import           Effects.Db               (runWithPool, withPool)
import           Models                   (AuthUser (..), EntityField (..),
                                           Unique (..), User)
import           Storage.Types            (KeyModelPair, MaybeKeyModelPair,
                                           StorageTypes (..))
import           Storage.Utils.PersistSql

checkConflictingAccount ::
     forall m env. (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> User
  -> m (Maybe User)
checkConflictingAccount SqlStorage user = do
  pool <- grab @ConnectionPool
  existingUser <- runWithPool pool $ getByValue user
  return $ entityVal <$> existingUser

createAccount ::
     forall m env. (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> User
  -> m (MaybeKeyModelPair User)
createAccount SqlStorage user = do
  pool <- grab @ConnectionPool
  createdUser <- runWithPool pool $ insertUniqueEntity user
  return $ maybeEntityToKeyRecordPair createdUser

findAccount ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Text
  -> m (Maybe (MaybeKeyModelPair AuthUser, KeyModelPair User))
findAccount SqlStorage userName = do
  pool <- grab @ConnectionPool
  authAccount <-
    runWithPool pool $
    select . from $ \(u `LeftOuterJoin` a) -> do
      on (just (u ^. UserId) ==. a ?. AuthUserUserId)
      where_ (u ^. UserUserName ==. val userName)
      limit 1
      return (a, u)
  return $
    (\(a, u) -> (maybeEntityToKeyRecordPair a, entityToKeyRecordPair u)) <$>
    listToMaybe authAccount

findAccountAlreadyLoggedIn ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Text
  -> m (MaybeKeyModelPair AuthUser)
findAccountAlreadyLoggedIn SqlStorage loginToken = do
  pool <- grab @ConnectionPool
  authAccount <-
    runWithPool pool $ selectFirst [AuthUserToken P.==. loginToken] []
  return $ maybeEntityToKeyRecordPair authAccount

logUserIn ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Key User
  -> Text
  -> m ()
logUserIn SqlStorage userid token = do
  withPool $ insertEntity $ AuthUser userid token
  return ()

logUserOut ::
     (MonadIO m, MonadReader env m, Has ConnectionPool env)
  => StorageTypes
  -> Text
  -> m ()
logUserOut SqlStorage token = do
  pool <- grab @ConnectionPool
  runWithPool pool $ deleteBy (UniqueAuthToken token)
