module Storage.Types where

import           Database.Persist.Sql (ConnectionPool, Key)

data StorageTypes =
  SqlStorage
  deriving (Eq, Show)

data PersistSqlStorage =
  PersistSqlStorage

class StorageType a

instance StorageType PersistSqlStorage

type KeyModelPair a = (Key a, a)

type MaybeKeyModelPair a = Maybe (KeyModelPair a)
