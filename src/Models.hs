{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Models where

import           Control.Monad.IO.Class (liftIO)
import           Data.Aeson
import           Data.Aeson.TH
import           Data.Text              (Text)
import           Database.Persist
import           Database.Persist.TH
import           GHC.Generics           (Generic (..))

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|
  Review json
    poster UserId
    reviewee UserId
    project ProjectId
    comment Text
    rating Double
    deriving Show
  Project json
    owner UserId
    title Text
    description Text
    type Text
    deriving Show
  ProjectTask json
    owner UserId
    contractor ContractorProfileId
    project ProjectId
    title Text
    isComplete Bool
    taskOrder Int
    subtasks [ProjectSubtask]
    deriving Show
  ProjectSubtask json
    -- project ProjectId
    title Text
    isComplete Bool
    taskOrder Int
    deriving Show
  User json
    name Text
    userName Text
    phoneNumber Text
    password Text
    projects  [Project] Maybe
    reviews  [Review] Maybe
    contractorProfile ContractorProfile Maybe
    UniqueUserName userName
    UniquePhoneNumber phoneNumber
    deriving Show
  AuthUser json
    userId UserId
    token Text
    -- tokenSecret Text
    UniqueAuthToken token
    deriving Show

  -- ProjectManager
  --   projects [ProjectId]
  --   deriving Show
  ContractorProfile json
    user UserId
    company Text
    jobsCompleted Int
    deriving Show
  -- +UserRole
  --   contractor ContractorId
  --   projectManager ProjectManagerId
  --   deriving Show
  |]
