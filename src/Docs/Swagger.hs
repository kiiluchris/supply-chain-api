{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving  #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE TypeOperators       #-}

module Docs.Swagger
  ( SwaggerSchemaUI
  , swaggerDocsBS
  , toSwagger
  , swaggerSchemaUIServer
  ) where

import           Control.Lens         hiding (Review)
import           Data.Aeson           (encode, toJSON)
import qualified Data.ByteString.Lazy as L
import qualified Data.Char            as Char (isLower, toLower)
import qualified Data.List            as List
import           Data.Swagger
import           Data.Text
import           Data.Typeable        (TypeRep, Typeable, typeRep)
import           Database.Esqueleto   (BackendKey (..), PersistEntity,
                                       SqlBackend)
import           GHC.Generics         (Generic)
import           GHC.TypeLits         (KnownSymbol, Symbol, symbolVal)
import           Servant              ((:<|>) (..), (:>), AuthProtect, Handler,
                                       Proxy (Proxy), Raw, Server)
import           Servant.Swagger
import           Servant.Swagger.UI

import           App.Types
import           Models
import           Storage.Filters

deriving instance Generic Review

deriving instance Generic User

deriving instance Generic Project

deriving instance Generic ProjectTask

deriving instance Generic ProjectSubtask

deriving instance Generic ContractorProfile

deriving instance Generic (Key Review)

deriving instance Generic (Key User)

deriving instance Generic (Key Project)

deriving instance Generic (Key ProjectTask)

deriving instance Generic (Key ProjectSubtask)

deriving instance Generic (Key ContractorProfile)

schemaPrefixStripper :: String -> String -> String
schemaPrefixStripper prefix propertyName =
  case List.stripPrefix prefix propertyName of
    Nothing     -> propertyName
    Just (p:ps) -> Char.toLower p : ps

schemaPrefixOptionsStripped :: String -> SchemaOptions
schemaPrefixOptionsStripped prefix =
  defaultSchemaOptions {fieldLabelModifier = schemaPrefixStripper prefix}

instance ToSchema ApiToken

instance ToSchema Review where
  declareNamedSchema =
    genericDeclareNamedSchema (schemaPrefixOptionsStripped "review")

instance ToSchema User where
  declareNamedSchema =
    genericDeclareNamedSchema (schemaPrefixOptionsStripped "user")

instance ToSchema Project where
  declareNamedSchema =
    genericDeclareNamedSchema (schemaPrefixOptionsStripped "project")

instance ToSchema ProjectTask where
  declareNamedSchema =
    genericDeclareNamedSchema (schemaPrefixOptionsStripped "projectTask")

instance ToSchema ProjectSubtask where
  declareNamedSchema =
    genericDeclareNamedSchema (schemaPrefixOptionsStripped "projectSubtask")

instance ToSchema ContractorProfile where
  declareNamedSchema =
    genericDeclareNamedSchema (schemaPrefixOptionsStripped "contractorProfile")

instance ToSchema a => ToSchema (ApiResponse a) where
  declareNamedSchema =
    genericDeclareNamedSchema (schemaPrefixOptionsStripped "apiRes")

instance ToSchema a => ToParamSchema (Key a) where
  toParamSchema _ = mempty & type_ ?~ SwaggerInteger

instance ToSchema a => ToSchema (Key a) where
  declareNamedSchema = pure . NamedSchema (Just "Key") . paramSchemaToSchema

instance ToSchema AuthRequest where
  declareNamedSchema =
    genericDeclareNamedSchema (schemaPrefixOptionsStripped "authRequest")

instance ToSchema UserRegister where
  declareNamedSchema =
    genericDeclareNamedSchema (schemaPrefixOptionsStripped "userReg")

instance ToSchema (BackendKey SqlBackend) where
  declareNamedSchema = pure . NamedSchema (Just "SQL") . paramSchemaToSchema

instance ToParamSchema (BackendKey SqlBackend) where
  toParamSchema _ = mempty

instance ToParamSchema (QueryFilter ReviewFilter) where
  toParamSchema _ =
    mempty & type_ ?~ SwaggerString & enum_ ?~
    [ "{\"page\": 1, \"page-size\": 10}"
    , "{\"page\": 1, \"page-size\": 10, \"q\": {\"poster\": 1, \"reviewee\": 10}}"
    ]

instance ToParamSchema (QueryFilter UserFilter) where
  toParamSchema _ =
    mempty & type_ ?~ SwaggerString & enum_ ?~
    [ "{\"page\": 1, \"page-size\": 10}"
    , "{\"page\": 1, \"page-size\": 10, \"q\": {\"name\": \"Name\", \"in-name\": \"a\"}}"
    ]

instance ToParamSchema (QueryFilter ProjectFilter) where
  toParamSchema _ =
    mempty & type_ ?~ SwaggerString & enum_ ?~
    [ "{\"page\": 1, \"page-size\": 10}"
    , "{\"page\": 1, \"page-size\": 10, \"q\": {\"owner\": 1, \"type\": \"Painting\"}}"
    ]

instance ToParamSchema (QueryFilter ProjectTaskFilter) where
  toParamSchema _ =
    mempty & type_ ?~ SwaggerString & enum_ ?~
    [ "{\"page\": 1, \"page-size\": 10}"
    , "{\"page\": 1, \"page-size\": 10, \"q\": {\"owner\": 1, \"project\": 1}}"
    ]

instance ToParamSchema (QueryFilter ContractorProfileFilter) where
  toParamSchema _ =
    mempty & type_ ?~ SwaggerString & enum_ ?~
    [ "{\"page\": 1, \"page-size\": 10}"
    , "{\"page\": 1, \"page-size\": 10, \"q\": {\"in-company\": \"Capc\"}}"
    ]

addParam :: Param -> Swagger -> Swagger
addParam param = allOperations . parameters %~ (Inline param :)

instance (KnownSymbol sym, HasSwagger api) =>
         HasSwagger (AuthProtect sym :> api) where
  toSwagger _ = toSwagger (Proxy @api) & addParam param
    where
      hname = pack $ symbolVal (Proxy @sym)
      authHeaderName = "Authorization"
      authHeaderDesc = "Authorization is using " <> hname
      param =
        mempty & name .~ authHeaderName & description ?~ authHeaderDesc & schema .~
        ParamOther headerSchema
      headerSchema =
        mempty & in_ .~ ParamHeader & paramSchema .~
        toParamSchema (Proxy @String)

swaggerDocsBS :: (HasSwagger a) => Proxy a -> L.ByteString
swaggerDocsBS = encode . toSwagger

type SwaggerAPI a = a :<|> SwaggerSchemaUI "swagger-ui" "swagger.json"
