module Pkg.Users.Removing.Endpoint where

import           Utils.Preludes.Endpoint (ApiResponse (..), AppM,
                                               AuthProtectData, StorageTypes,
                                               User, UserId)
import           Pkg.Users.Removing.Service

deleteR :: StorageTypes -> AuthProtectData -> UserId -> AppM (ApiResponse User)
deleteR s auth rid = do
  deletedUser <- userDeleter s rid
  case deletedUser of
    Left err -> return $ ApiResponse err Nothing
    Right _  -> return $ ApiResponse "User successfully deleted" Nothing
