{-# LANGUAGE FlexibleContexts #-}

module Pkg.Users.Removing.Service where

import           Utils.Preludes.Service (ConnectionPool, Has,
                                              MonadBaseControl, MonadIO,
                                              MonadReader, StorageTypes, Text,
                                              User, UserId)
import           Storage.Users.Functions

userDeleter ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> UserId
  -> m (Either Text ())
userDeleter s uid = do
  userOpt <- retrieveUser s uid
  case userOpt of
    Nothing -> return $ Left "User does not exist"
    Just _ -> do
      deleteUser s uid
      return $ Right ()
