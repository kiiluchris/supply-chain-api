{-# LANGUAGE FlexibleContexts #-}

module Pkg.Users.Retrieving.Service where

import           Utils.Preludes.Service (ConnectionPool, Has,
                                              MonadBaseControl, MonadIO,
                                              MonadReader, StorageTypes, Text,
                                              User, UserId)
import           Storage.Users.Functions

userRetriever ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> UserId
  -> m (Maybe User)
userRetriever s uid = do
  userOpt <- retrieveUser s uid
  return $ fmap snd userOpt
