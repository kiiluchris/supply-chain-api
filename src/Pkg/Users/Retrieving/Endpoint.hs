module Pkg.Users.Retrieving.Endpoint where

import           Utils.Preludes.Endpoint (ApiResponse (..), AppM,
                                               AuthProtectData, StorageTypes,
                                               User, UserId, throw404)
import           Pkg.Users.Retrieving.Service

getR :: StorageTypes -> AuthProtectData -> UserId -> AppM User
getR s auth uid = do
  userOpt <- userRetriever s uid
  maybe (throw404 "User not found") return userOpt
