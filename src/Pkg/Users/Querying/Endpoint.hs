module Pkg.Users.Querying.Endpoint where

import           Pkg.Users.Querying.Service
import           Utils.Preludes.Endpoint    (ApiResponse (..), AppM,
                                             AuthProtectData, QueryFilter,
                                             StorageTypes, User, UserFilter)

listR ::
     StorageTypes
  -> AuthProtectData
  -> Maybe (QueryFilter UserFilter)
  -> AppM [User]
listR s auth = userQuerant s
