{-# LANGUAGE FlexibleContexts #-}

module Pkg.Users.Querying.Service where

import           Storage.Users.Functions
import           Utils.Preludes.Service  (ConnectionPool, Has, MonadBaseControl,
                                          MonadIO, MonadReader, QueryFilter,
                                          StorageTypes, User, UserFilter)

userQuerant ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> Maybe (QueryFilter UserFilter)
  -> m [User]
userQuerant = queryUsers
