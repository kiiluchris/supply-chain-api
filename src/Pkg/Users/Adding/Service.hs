{-# LANGUAGE FlexibleContexts #-}

module Pkg.Users.Adding.Service where

import           Utils.Preludes.Service (ConnectionPool, Has,
                                              MonadBaseControl, MonadIO,
                                              MonadReader, StorageTypes, Text,
                                              User, UserId)
import           Storage.Users.Functions
import           Storage.Users.Utils

userCreator ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> User
  -> m (Either Text ())
userCreator s user = do
  userOpt <- saveUser s user
  case userOpt of
    Just (_, u) -> return $ Left $ compareAccountDetails user u
    Nothing     -> return $ Right ()
