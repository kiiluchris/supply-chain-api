module Pkg.Users.Adding.Endpoint where

import           Utils.Preludes.Endpoint (ApiResponse (..), AppM,
                                               AuthProtectData, StorageTypes,
                                               User, UserId)
import           Pkg.Users.Adding.Service

createR :: StorageTypes -> AuthProtectData -> User -> AppM (ApiResponse User)
createR s auth reqBody = do
  existingUser <- userCreator s reqBody
  case existingUser of
    Left err -> return $ ApiResponse err Nothing
    Right _  -> return $ ApiResponse "User successfully created" Nothing
