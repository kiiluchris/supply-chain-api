{-# LANGUAGE DataKinds #-}

module Pkg.Users.Server where

import           Utils.Preludes.Server
import           Pkg.Users.Adding.Endpoint
import           Pkg.Users.Querying.Endpoint
import           Pkg.Users.Removing.Endpoint
import           Pkg.Users.Retrieving.Endpoint
import           Pkg.Users.Updating.Endpoint

type UserApi = GenericApiAuth "users" User UserFilter

userApp :: ServerT UserApi AppM
userApp =
  listR SqlStorage :<|> createR SqlStorage :<|> getR SqlStorage :<|>
  updateR SqlStorage :<|>
  deleteR SqlStorage
