{-# LANGUAGE FlexibleContexts #-}

module Pkg.Users.Updating.Service where

import           Utils.Preludes.Service (ConnectionPool, Has,
                                              MonadBaseControl, MonadIO,
                                              MonadReader, StorageTypes, Text,
                                              User (..), UserId)
import           Storage.Users.Functions

userUpdater ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> UserId
  -> User
  -> m (Either Text ())
userUpdater s uid User { userName = name
                       , userUserName = username
                       , userPhoneNumber = phone
                       } = do
  userOpt <- retrieveUser s uid
  case userOpt of
    Nothing -> return $ Left "User does not exist"
    Just (_, u) -> do
      let updates =
            u
              { userName = name
              , userUserName = username
              , userPhoneNumber = phone
              }
      updateUser s uid updates
      return $ Right ()
