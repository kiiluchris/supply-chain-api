module Pkg.Users.Updating.Endpoint where

import           Utils.Preludes.Endpoint (ApiResponse (..), AppM,
                                               AuthProtectData, StorageTypes,
                                               User, UserId)
import           Pkg.Users.Updating.Service

updateR ::
     StorageTypes
  -> AuthProtectData
  -> UserId
  -> User
  -> AppM (ApiResponse User)
updateR s auth uid reqBody = do
  updatedUser <- userUpdater s uid reqBody
  case updatedUser of
    Left err -> return $ ApiResponse err Nothing
    Right _  -> return $ ApiResponse "User successfully updated" Nothing
