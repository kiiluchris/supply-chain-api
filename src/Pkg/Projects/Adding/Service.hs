{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NamedFieldPuns   #-}

module Pkg.Projects.Adding.Service where

import           Storage.Projects.Functions
import           Storage.Users.Functions    (retrieveUser)
import           Utils.Preludes.Service     (ConnectionPool, Has, Has, Has,
                                             MonadBaseControl, MonadIO,
                                             MonadReader, Project (..),
                                             StorageTypes, Text, runIfRetrieved)

projectCreator ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> Project
  -> m (Either Text Project)
projectCreator s project@Project {projectOwner} =
  runIfRetrieved "User does not exist" (retrieveUser s projectOwner) $ do
  (_, createdProject) <- saveProject s project
  return $ Right createdProject
