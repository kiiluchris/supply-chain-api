module Pkg.Projects.Adding.Endpoint where

import           Pkg.Projects.Adding.Service
import           Utils.Preludes.Endpoint     (ApiResponse (..), AppM,
                                              AuthProtectData, Project,
                                              StorageTypes)

createR ::
     StorageTypes -> AuthProtectData -> Project -> AppM (ApiResponse Project)
createR s auth reqBody = do
  project <- projectCreator s reqBody
  case project of
    Left err -> return $ ApiResponse err Nothing
    Right p  -> return $ ApiResponse "Project created" (Just p)
