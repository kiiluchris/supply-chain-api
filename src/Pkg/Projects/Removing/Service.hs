{-# LANGUAGE FlexibleContexts #-}

module Pkg.Projects.Removing.Service where

import           Utils.Preludes.Service (ConnectionPool, Has,
                                              MonadBaseControl, MonadIO,
                                              MonadReader, Project, ProjectId,
                                              StorageTypes, Text)
import           Storage.Projects.Functions

projectDeleter ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> ProjectId
  -> m (Either Text ())
projectDeleter s pid = do
  projectOpt <- retrieveProject s pid
  case projectOpt of
    Nothing -> return $ Left "Project does not exist"
    Just _ -> do
      deleteProject s pid
      return $ Right ()
