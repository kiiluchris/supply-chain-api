module Pkg.Projects.Removing.Endpoint where

import           Pkg.Projects.Removing.Service
import           Utils.Preludes.Endpoint  (ApiResponse (..), AppM,
                                                AuthProtectData, Project,
                                                ProjectId, StorageTypes)

deleteR ::
     StorageTypes -> AuthProtectData -> ProjectId -> AppM (ApiResponse Project)
deleteR s auth projectId = do
  projectOpt <- projectDeleter s projectId
  case projectOpt of
    Right _  -> return $ ApiResponse "Project successfully deleted" Nothing
    Left err -> return $ ApiResponse err Nothing
