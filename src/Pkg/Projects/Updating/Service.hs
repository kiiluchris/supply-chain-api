{-# LANGUAGE FlexibleContexts #-}

module Pkg.Projects.Updating.Service where

import           Utils.Preludes.Service (ConnectionPool, Has,
                                              MonadBaseControl, MonadIO,
                                              MonadReader, Project, Project,
                                              ProjectId, StorageTypes, Text)
import           Storage.Projects.Functions

projectUpdater ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> ProjectId
  -> Project
  -> m (Either Text ())
projectUpdater s pid updateValue = do
  projectOpt <- retrieveProject s pid
  case projectOpt of
    Nothing -> return $ Left "Project does not exist"
    Just _ -> do
      updateProject s pid updateValue
      return $ Right ()
