module Pkg.Projects.Updating.Endpoint where

import           Pkg.Projects.Updating.Service
import           Utils.Preludes.Endpoint  (ApiResponse (..), AppM,
                                                AuthProtectData, Project,
                                                ProjectId, StorageTypes)

updateR ::
     StorageTypes
  -> AuthProtectData
  -> ProjectId
  -> Project
  -> AppM (ApiResponse Project)
updateR s auth pid reqBody = do
  updatedValue <- projectUpdater s pid reqBody
  case updatedValue of
    Left err -> return $ ApiResponse "Project does not exist" Nothing
    Right _  -> return $ ApiResponse "Project updated" Nothing
