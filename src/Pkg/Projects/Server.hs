{-# LANGUAGE DataKinds #-}

module Pkg.Projects.Server where

import           Pkg.Projects.Adding.Endpoint
import           Pkg.Projects.Querying.Endpoint
import           Pkg.Projects.Removing.Endpoint
import           Pkg.Projects.Retrieving.Endpoint
import           Pkg.Projects.Updating.Endpoint
import           Utils.Preludes.Server

type ProjectApi = GenericApiAuth "projects" Project ProjectFilter

projectApp :: ServerT ProjectApi AppM
projectApp =
  listR SqlStorage :<|> createR SqlStorage :<|> getR SqlStorage :<|>
  updateR SqlStorage :<|>
  deleteR SqlStorage
