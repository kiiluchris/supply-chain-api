{-# LANGUAGE FlexibleContexts #-}

module Pkg.Projects.Querying.Service where

import           Storage.Projects.Functions
import           Utils.Preludes.Service     (ConnectionPool, Has,
                                             MonadBaseControl, MonadIO,
                                             MonadReader, Project,
                                             ProjectFilter, QueryFilter,
                                             StorageTypes)

projectFinder ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> Maybe (QueryFilter ProjectFilter)
  -> m [Project]
projectFinder = queryProjects
