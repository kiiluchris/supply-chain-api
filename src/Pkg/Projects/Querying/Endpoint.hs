module Pkg.Projects.Querying.Endpoint where

import           Pkg.Projects.Querying.Service
import           Utils.Preludes.Endpoint       (AppM, AuthProtectData, Project,
                                                ProjectFilter, QueryFilter,
                                                StorageTypes)

listR ::
     StorageTypes
  -> AuthProtectData
  -> Maybe (QueryFilter ProjectFilter)
  -> AppM [Project]
listR s auth = projectFinder s
