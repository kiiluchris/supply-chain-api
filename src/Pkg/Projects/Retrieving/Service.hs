{-# LANGUAGE FlexibleContexts #-}

module Pkg.Projects.Retrieving.Service where

import           Utils.Preludes.Service (ConnectionPool, Has,
                                              MonadBaseControl, MonadIO,
                                              MonadReader, Project, ProjectId,
                                              StorageTypes)
import           Storage.Projects.Functions

projectRetriever ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> ProjectId
  -> m (Maybe Project)
projectRetriever s pid = do
  projectOpt <- retrieveProject s pid
  return $ fmap snd projectOpt
