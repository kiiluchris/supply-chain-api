module Pkg.Projects.Retrieving.Endpoint where

import           Pkg.Projects.Retrieving.Service
import           Utils.Preludes.Endpoint    (ApiResponse (..), AppM,
                                                  AuthProtectData, Project,
                                                  ProjectId, StorageTypes,
                                                  throw404)

getR :: StorageTypes -> AuthProtectData -> ProjectId -> AppM Project
getR s auth reqBody = do
  projectOpt <- projectRetriever s reqBody
  maybe (throw404 "Project not found") return projectOpt
