module Pkg.UserProjects.Retrieving.Endpoint where

import           Utils.Preludes.Endpoint        (ApiResponse (..), AppM,
                                                      AuthProtectData, Project,
                                                      ProjectId, StorageTypes,
                                                      UserId, throw401,
                                                      throw404)
import           Pkg.UserProjects.Retrieving.Service

getR :: StorageTypes -> AuthProtectData -> UserId -> ProjectId -> AppM Project
getR s auth uid pid = do
  projectOpt <- userProjectsRetriever s uid pid
  maybe (throw404 "Project not found") return projectOpt
