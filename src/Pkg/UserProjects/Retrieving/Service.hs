{-# LANGUAGE FlexibleContexts #-}

module Pkg.UserProjects.Retrieving.Service where

import           Utils.Preludes.Service (ConnectionPool, Has,
                                              MonadBaseControl, MonadIO,
                                              MonadReader, Project (..),
                                              ProjectId, StorageTypes, Text,
                                              UserId)
import           Storage.Projects.Functions

userProjectsRetriever ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> UserId
  -> ProjectId
  -> m (Maybe Project)
userProjectsRetriever s uid pid = do
  existingProject <- retrieveProject s pid
  case existingProject of
    Just (_, p@Project {projectOwner = owner})
      | owner == uid -> return $ Just p
    _ -> return Nothing
