module Pkg.UserProjects.Adding.Endpoint where

import           Pkg.UserProjects.Adding.Service
import           Utils.Preludes.Endpoint         (ApiResponse (..), AppM,
                                                  AuthProtectData, Project,
                                                  ProjectId, StorageTypes,
                                                  UserId, throw401, throw404)

createR ::
     StorageTypes
  -> AuthProtectData
  -> UserId
  -> Project
  -> AppM (ApiResponse Project)
createR s auth uid reqBody = do
  createdProject <- userProjectsCreator s uid reqBody
  case createdProject of
    Left err -> return $ ApiResponse err Nothing
    Right p  -> return $ ApiResponse "Project is created" (Just p)
