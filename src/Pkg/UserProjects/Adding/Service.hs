{-# LANGUAGE FlexibleContexts #-}

module Pkg.UserProjects.Adding.Service where

import           Storage.Projects.Functions
import           Storage.Users.Functions    (retrieveUser)
import           Utils.Preludes.Service     (ConnectionPool, Has,
                                             MonadBaseControl, MonadIO,
                                             MonadReader, Project (..),
                                             ProjectId, StorageTypes, Text,
                                             UserId, runIfRetrieved)

userProjectsCreator ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> UserId
  -> Project
  -> m (Either Text Project)
userProjectsCreator s uid project = do
  let p = project {projectOwner = uid}
  runIfRetrieved "User does not exist" (retrieveUser s uid) $ do
    (_, createdProject) <- saveProject s p
    return $ Right createdProject
