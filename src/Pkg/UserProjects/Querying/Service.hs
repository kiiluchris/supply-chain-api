{-# LANGUAGE FlexibleContexts #-}


module Pkg.UserProjects.Querying.Service where

import           Storage.Projects.Functions
import           Utils.Preludes.Service     (ConnectionPool, Has,
                                             MonadBaseControl, MonadIO,
                                             MonadReader, Project (..),
                                             ProjectFilter (..), ProjectId,
                                             QueryFilter (..), StorageTypes,
                                             Text, UserId, defaultFilter)

userProjectsQuerant ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> UserId
  -> Maybe (QueryFilter ProjectFilter)
  -> m [Project]
userProjectsQuerant s uid filters =
  queryProjects s (setQueryFilterQMaybe uid filters)
  where
    setProjectOwner uid p = p {projectFilterOwner = Just uid}
    setProjectOwnerMaybe uid Nothing =
      setProjectOwnerMaybe uid (Just defaultFilter)
    setProjectOwnerMaybe uid p@(Just _) = setProjectOwner uid <$> p
    setQueryFilterQ p q = q {queryFilterQ = p}
    setQueryFilterQMaybe uid Nothing =
      setQueryFilterQMaybe uid (Just defaultFilter)
    setQueryFilterQMaybe uid (Just queryFilter) =
      Just $
      setQueryFilterQ
        (setProjectOwnerMaybe uid $ queryFilterQ queryFilter)
        queryFilter
