module Pkg.UserProjects.Querying.Endpoint where

import           Pkg.UserProjects.Querying.Service
import           Utils.Preludes.Endpoint           (ApiResponse (..), AppM,
                                                    AuthProtectData, Project,
                                                    ProjectFilter, ProjectId,
                                                    QueryFilter, StorageTypes,
                                                    UserId, throw401, throw404)

listR ::
     StorageTypes
  -> AuthProtectData
  -> UserId
  -> Maybe (QueryFilter ProjectFilter)
  -> AppM [Project]
listR s auth = userProjectsQuerant s
