module Pkg.UserProjects.Updating.Endpoint where

import           Utils.Preludes.Endpoint      (ApiResponse (..), AppM,
                                                    AuthProtectData, Project,
                                                    ProjectId, StorageTypes,
                                                    UserId, throw401, throw404)
import           Pkg.UserProjects.Updating.Service

updateR ::
     StorageTypes
  -> AuthProtectData
  -> UserId
  -> ProjectId
  -> Project
  -> AppM (ApiResponse Project)
updateR s auth uid pid project = do
  updatedProject <- userProjectsUpdater s uid pid project
  case updatedProject of
    Left err -> return $ ApiResponse err Nothing
    Right _  -> return $ ApiResponse "Project successfully updated" Nothing
