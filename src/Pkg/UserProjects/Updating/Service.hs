{-# LANGUAGE FlexibleContexts #-}

module Pkg.UserProjects.Updating.Service where

import           Utils.Preludes.Service (ConnectionPool, Has,
                                              MonadBaseControl, MonadIO,
                                              MonadReader, Project (..),
                                              ProjectId, StorageTypes, Text,
                                              UserId)
import           Storage.Projects.Functions

userProjectsUpdater ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> UserId
  -> ProjectId
  -> Project
  -> m (Either Text ())
userProjectsUpdater s uid pid updateData = do
  existingProject <- retrieveProject s pid
  runIfProjectExists existingProject $ \p -> do
    updateProject s pid updateData
    return $ Right ()
  where
    runIfProjectExists (Just (_, p@Project {projectOwner = owner})) action
      | owner == uid = action p
    runIfProjectExists _ _ = return $ Left "Project does not exist"
