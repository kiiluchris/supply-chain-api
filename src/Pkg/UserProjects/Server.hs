{-# LANGUAGE DataKinds #-}

module Pkg.UserProjects.Server where

import           Pkg.UserProjects.Adding.Endpoint
import           Pkg.UserProjects.Querying.Endpoint
import           Pkg.UserProjects.Removing.Endpoint
import           Pkg.UserProjects.Retrieving.Endpoint
import           Pkg.UserProjects.Updating.Endpoint
import           Utils.Preludes.Server

type UserProjectApi
   = GenericApiNestedAuth "users" UserId "projects" Project ProjectFilter

userProjectApp :: ServerT UserProjectApi AppM
userProjectApp auth uid =
  listR SqlStorage auth uid :<|> createR SqlStorage auth uid :<|>
  getR SqlStorage auth uid :<|>
  updateR SqlStorage auth uid :<|>
  deleteR SqlStorage auth uid
