{-# LANGUAGE FlexibleContexts #-}

module Pkg.UserProjects.Removing.Service where

import           Utils.Preludes.Service (ConnectionPool, Has,
                                              MonadBaseControl, MonadIO,
                                              MonadReader, Project (..),
                                              ProjectId, StorageTypes, Text,
                                              UserId)
import           Storage.Projects.Functions

userProjectsDeleter ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> UserId
  -> ProjectId
  -> m (Either Text ())
userProjectsDeleter s uid pid = do
  existingProjectOpt <- retrieveProject s pid
  case existingProjectOpt of
    Just (_, Project {projectOwner = owner})
      | owner == uid -> return $ Right ()
    _ -> return $ Left "Project does not exist"
