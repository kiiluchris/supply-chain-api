module Pkg.UserProjects.Removing.Endpoint where

import           Utils.Preludes.Endpoint      (ApiResponse (..), AppM,
                                                    AuthProtectData, Project,
                                                    ProjectId, StorageTypes,
                                                    UserId, throw401, throw404)
import           Pkg.UserProjects.Removing.Service

deleteR ::
     StorageTypes
  -> AuthProtectData
  -> UserId
  -> ProjectId
  -> AppM (ApiResponse Project)
deleteR s auth uid pid = do
  deletedProject <- userProjectsDeleter s uid pid
  case deletedProject of
    Left err -> return $ ApiResponse err Nothing
    Right _  -> return $ ApiResponse "Project deleted" Nothing
