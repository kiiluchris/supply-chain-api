{-# LANGUAGE DataKinds        #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# LANGUAGE TypeOperators    #-}

module Pkg.Server where

import           Servant                       ((:<|>) (..), HasServer,
                                                Proxy (..), Server, ServerT)

import           App.AppM                      (AppM, hoistAppMServerWithCtx)
import           App.Env.Data                  (AppEnv)
import           Pkg.Accounts.Server           (AccountApi, accountApp)
import           Pkg.ContractorProfiles.Server (ContractorProfileApi,
                                                contractorProfileApp)
import           Pkg.Projects.Server           (ProjectApi, projectApp)
import           Pkg.Reviews.Server            (ReviewApi, reviewApp)
import           Pkg.UserProjects.Server       (UserProjectApi, userProjectApp)
import           Pkg.UserProjectTasks.Server   (UserProjectTaskApi,
                                                userProjectTaskApp)
import           Pkg.Users.Server              (UserApi, userApp)

type RouteApi
   = AccountApi :<|> ReviewApi :<|> ContractorProfileApi :<|> ProjectApi :<|> UserProjectApi :<|> UserApi :<|> UserProjectTaskApi

mainApp :: ServerT RouteApi AppM
mainApp =
  accountApp :<|> reviewApp :<|> contractorProfileApp :<|> projectApp :<|>
  userProjectApp :<|>
  userApp :<|>
  userProjectTaskApp

mainAppServer ::
     forall (ctx :: [*]). (HasServer RouteApi ctx)
  => AppEnv
  -> Proxy ctx
  -> Server RouteApi
mainAppServer env contextProxy =
  hoistAppMServerWithCtx env mainApi contextProxy mainApp

mainApi :: Proxy RouteApi
mainApi = Proxy
