{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module Pkg.Accounts.Server where

import           Servant                        ((:<|>) (..), (:>), JSON, Post,
                                                 Proxy (Proxy), ReqBody,
                                                 ServerT)

import           App.Types                      (AuthRequest, AuthResponse,
                                                 UserRegister)
import           App.AppM                       (AppM)
import           Models                         (Review)
import           Pkg.Accounts.Login.Endpoint    (loginR)
import           Pkg.Accounts.Logout.Endpoint   (logoutR)
import           Pkg.Accounts.Register.Endpoint (registerR)
import           Utils.Routing              (GenericApiAuth, JwtAuthProtect)
import           Storage.Filters                (ReviewFilter)
import           Storage.Types                  (StorageTypes (..))

type AccountApi
   = "auth" :> (("login" :> ReqBody '[ JSON] AuthRequest :> Post '[ JSON] AuthResponse) :<|> ("logout" :> JwtAuthProtect :> Post '[ JSON] AuthResponse) :<|> ("register" :> ReqBody '[ JSON] UserRegister :> Post '[ JSON] AuthResponse))

accountApp :: ServerT AccountApi AppM
accountApp = loginR SqlStorage :<|> logoutR SqlStorage :<|> registerR SqlStorage
