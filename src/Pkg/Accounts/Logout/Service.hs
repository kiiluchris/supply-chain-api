{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}


module Pkg.Accounts.Logout.Service where

import           Data.Text                   (Text)

import           Utils.Preludes.Service (ConnectionPool, Has,
                                              MonadBaseControl, MonadIO,
                                              MonadReader, StorageTypes)
import           Storage.Accounts.Functions  (findAccountAlreadyLoggedIn,
                                              logUserOut)

authorizationRevoker ::
     ( MonadReader env m
     , MonadIO m
     , MonadBaseControl IO m
     , Has ConnectionPool env
     )
  => StorageTypes
  -> Text
  -> m Text
authorizationRevoker s authToken = do
  authDetailsOpt <- findAccountAlreadyLoggedIn s authToken
  case authDetailsOpt of
    Just (_authId, _authUser) -> do
      logUserOut s authToken
      return "User has been logged out"
    _ -> return "User was not logged in"
