module Pkg.Accounts.Logout.Endpoint where

import           App.Types                    (ApiResponse (..), AuthResponse)
import           Pkg.Accounts.Logout.Service
import           Utils.Preludes.Endpoint (AppM, AuthProtectData,
                                               StorageTypes)

logoutR :: StorageTypes -> AuthProtectData -> AppM AuthResponse
logoutR s (_, _, token) = do
  logoutMessage <- authorizationRevoker s token
  return $ ApiResponse logoutMessage Nothing
