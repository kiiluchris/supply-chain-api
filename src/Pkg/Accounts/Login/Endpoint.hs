module Pkg.Accounts.Login.Endpoint where

import           App.Types                    (ApiResponse (..), ApiToken (..),
                                               AuthRequest (..), AuthResponse)
import           Pkg.Accounts.Login.Service
import           Utils.Preludes.Endpoint (AppM, StorageTypes)

loginR :: StorageTypes -> AuthRequest -> AppM AuthResponse
loginR _ (AuthRequest _name Nothing _token) =
  return $ ApiResponse "Failure: No password given" Nothing
loginR _ (AuthRequest Nothing _pswd _token) =
  return $ ApiResponse "Failure: No username given" Nothing
loginR s (AuthRequest (Just name) (Just pswd) _token) = do
  updatedReview <- authorizationGranter s (name, pswd)
  return $
    case updatedReview of
      Left err -> ApiResponse err Nothing
      Right authToken ->
        ApiResponse "Authentication successful" (Just authToken)
