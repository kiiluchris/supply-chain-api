{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}


module Pkg.Accounts.Login.Service where

import           Data.Text                   (Text)

import           App.Types                   (ApiToken (..))
import           Effects.JwtAuth          (Signer, mkToken)
import           Utils.Preludes.Service (AuthUser (..), ConnectionPool,
                                              Has, MonadBaseControl, MonadIO,
                                              MonadReader, StorageTypes,
                                              User (..))
import           Storage.Accounts.Functions  (findAccount, logUserIn,
                                              logUserOut)
import           Utils.Password

authorizationGranter ::
     ( MonadReader env m
     , MonadIO m
     , MonadBaseControl IO m
     , Has ConnectionPool env
     , Has Signer env
     )
  => StorageTypes
  -> (Text, Text)
  -> m (Either Text ApiToken)
authorizationGranter s (username, pswd) = do
  existingAccount <- findAccount s username
  checkAccountFound existingAccount
  where
    checkAccountFound (Just details) = authenticateUser details
    checkAccountFound Nothing        = return $ Left "Account does not exist"
    authenticateUser (auth, (userid, user)) =
      if not $ verifyUserPassword user pswd
        then return $ Left "Invalid password"
        else generateUserToken auth userid user
    generateUserToken (Just (authid, auth)) uid user = do
      logUserOut s $ authUserToken auth
      generateUserToken Nothing uid user
    generateUserToken Nothing uid User {userUserName = name} = do
      token <- mkToken name
      logUserIn s uid token
      (return . Right . ApiToken) token
