{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Pkg.Accounts.Register.Service where

import           Data.Text                  (Text)

import           App.Types                  (UserRegister (..))
import           Storage.Accounts.Functions (checkConflictingAccount,
                                             createAccount)
import           Storage.Users.Utils
import           Utils.Maybe                (maybeToEither)
import           Utils.Password             (hashPswd)
import           Utils.Preludes.Service     (ConnectionPool, Has,
                                             MonadBaseControl, MonadIO,
                                             MonadReader, StorageTypes,
                                             User (..))

accountCreator ::
     ( MonadReader env m
     , MonadIO m
     , MonadBaseControl IO m
     , Has ConnectionPool env
     )
  => StorageTypes
  -> UserRegister
  -> m (Either Text User)
accountCreator s (UserRegister name username pswd phone) = do
  let user =
        User
          { userName = name
          , userUserName = username
          , userPhoneNumber = phone
          , userPassword = ""
          , userProjects = Nothing
          , userReviews = Nothing
          , userContractorProfile = Nothing
          }
  existingAcc <- checkConflictingAccount s user
  handleExistingAccount existingAcc user pswd
  where
    handleExistingAccount (Just acc) user _ =
      return . Left $ compareAccountDetails user acc
    handleExistingAccount Nothing user pswd = do
      hashedPswdOpt <- hashPswd pswd
      handleHashedPassword hashedPswdOpt user
    handleHashedPassword Nothing user =
      return . Left $ "Password hash could not be generated"
    handleHashedPassword (Just hashedPswd) user = do
      createdUserOpt <- createAccount s (user {userPassword = hashedPswd})
      return $ maybeToEither "Account already exists" (fmap snd createdUserOpt)
