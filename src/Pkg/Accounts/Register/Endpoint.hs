module Pkg.Accounts.Register.Endpoint where

import           App.Types                     (ApiResponse (..), AuthResponse,
                                                UserRegister)
import           Pkg.Accounts.Register.Service
import           Utils.Preludes.Endpoint  (AppM, StorageTypes)

registerR :: StorageTypes -> UserRegister -> AppM AuthResponse
registerR s user = do
  createdUser <- accountCreator s user
  return $
    case createdUser of
      Left err  -> ApiResponse err Nothing
      Right acc -> ApiResponse "Account created" Nothing
