{-# LANGUAGE FlexibleContexts #-}

module Pkg.ContractorProfiles.Removing.Service where

import           Utils.Preludes.Service          (ConnectionPool,
                                                       ContractorProfileId, Has,
                                                       MonadBaseControl,
                                                       MonadIO, MonadReader,
                                                       StorageTypes, Text)
import           Storage.ContractorProfiles.Functions

contractorProfilesDeleter ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> ContractorProfileId
  -> m (Either Text ())
contractorProfilesDeleter s pid = do
  existingProfile <- retrieveContractorProfile s pid
  case existingProfile of
    Nothing -> return $ Left "Profile does not exist"
    Just _ -> do
      deleteContractorProfile s pid
      return $ Right ()
