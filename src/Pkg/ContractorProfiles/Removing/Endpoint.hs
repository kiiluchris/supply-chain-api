module Pkg.ContractorProfiles.Removing.Endpoint where

import           Pkg.ContractorProfiles.Removing.Service
import           Utils.Preludes.Endpoint            (ApiResponse (..),
                                                          AppM, AuthProtectData,
                                                          ContractorProfile,
                                                          ContractorProfileId,
                                                          StorageTypes)

deleteR ::
     StorageTypes
  -> AuthProtectData
  -> ContractorProfileId
  -> AppM (ApiResponse ContractorProfile)
deleteR s auth pid = do
  profileDeleted <- contractorProfilesDeleter s pid
  case profileDeleted of
    Left err -> return $ ApiResponse err Nothing
    Right _  -> return $ ApiResponse "Profile deleted" Nothing
