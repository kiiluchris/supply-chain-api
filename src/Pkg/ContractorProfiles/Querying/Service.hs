{-# LANGUAGE FlexibleContexts #-}

module Pkg.ContractorProfiles.Querying.Service where

import           Storage.ContractorProfiles.Functions
import           Utils.Preludes.Service               (ConnectionPool,
                                                       ContractorProfile,
                                                       ContractorProfileFilter,
                                                       Has, MonadBaseControl,
                                                       MonadIO, MonadReader,
                                                       QueryFilter,
                                                       StorageTypes)

contractorProfilesQuerant ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> Maybe (QueryFilter ContractorProfileFilter)
  -> m [ContractorProfile]
contractorProfilesQuerant = queryContractorProfiles
