module Pkg.ContractorProfiles.Querying.Endpoint where

import           Pkg.ContractorProfiles.Querying.Service
import           Utils.Preludes.Endpoint                 (AppM, AuthProtectData,
                                                          ContractorProfile,
                                                          ContractorProfileFilter,
                                                          QueryFilter,
                                                          StorageTypes)

listR ::
     StorageTypes
  -> AuthProtectData
  -> Maybe (QueryFilter ContractorProfileFilter)
  -> AppM [ContractorProfile]
listR s auth = contractorProfilesQuerant s
