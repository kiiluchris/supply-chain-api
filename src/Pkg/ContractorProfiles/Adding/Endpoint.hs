module Pkg.ContractorProfiles.Adding.Endpoint where

import           Pkg.ContractorProfiles.Adding.Service
import           Utils.Preludes.Endpoint               (ApiResponse (..), AppM,
                                                        AuthProtectData,
                                                        ContractorProfile,
                                                        StorageTypes)

createR ::
     StorageTypes
  -> AuthProtectData
  -> ContractorProfile
  -> AppM (ApiResponse ContractorProfile)
createR s auth reqBody = do
  createdProfile <- contractorProfilesCreator s reqBody
  case createdProfile of
    Left err -> return $ ApiResponse err Nothing
    Right p  -> return $ ApiResponse "Successfully created profile" (Just p)
