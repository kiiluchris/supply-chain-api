{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NamedFieldPuns   #-}

module Pkg.ContractorProfiles.Adding.Service where

import           Storage.ContractorProfiles.Functions
import           Storage.Users.Functions              (retrieveUser)
import           Utils.Preludes.Service               (ConnectionPool,
                                                       ContractorProfile (..),
                                                       Has, MonadBaseControl,
                                                       MonadIO, MonadReader,
                                                       StorageTypes, Text,
                                                       runIfRetrieved)

contractorProfilesCreator ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> ContractorProfile
  -> m (Either Text ContractorProfile)
contractorProfilesCreator s profile@ContractorProfile {contractorProfileUser} =
  runIfRetrieved "User does not exist" (retrieveUser s contractorProfileUser) $ do
  (_, existingProfile) <- saveContractorProfile s profile
  return $ Right existingProfile
