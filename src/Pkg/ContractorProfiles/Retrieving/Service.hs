{-# LANGUAGE FlexibleContexts #-}

module Pkg.ContractorProfiles.Retrieving.Service where

import           Utils.Preludes.Service          (ConnectionPool,
                                                       ContractorProfile,
                                                       ContractorProfileId, Has,
                                                       MonadBaseControl,
                                                       MonadIO, MonadReader,
                                                       StorageTypes)
import           Storage.ContractorProfiles.Functions

contractorProfilesRetriever ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> ContractorProfileId
  -> m (Maybe ContractorProfile)
contractorProfilesRetriever s pid = do
  profile <- retrieveContractorProfile s pid
  return $ fmap snd profile
