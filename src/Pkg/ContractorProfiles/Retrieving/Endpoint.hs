module Pkg.ContractorProfiles.Retrieving.Endpoint where

import           Pkg.ContractorProfiles.Retrieving.Service
import           Utils.Preludes.Endpoint              (AppM,
                                                            AuthProtectData,
                                                            ContractorProfile,
                                                            ContractorProfileId,
                                                            StorageTypes,
                                                            throw404)

getR ::
     StorageTypes
  -> AuthProtectData
  -> ContractorProfileId
  -> AppM ContractorProfile
getR s auth pid = do
  profile <- contractorProfilesRetriever s pid
  maybe (throw404 "Profile not found") return profile
