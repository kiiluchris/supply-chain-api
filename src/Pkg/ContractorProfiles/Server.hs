{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module Pkg.ContractorProfiles.Server where

import           Pkg.ContractorProfiles.Adding.Endpoint
import           Pkg.ContractorProfiles.Querying.Endpoint
import           Pkg.ContractorProfiles.Removing.Endpoint
import           Pkg.ContractorProfiles.Retrieving.Endpoint
import           Pkg.ContractorProfiles.Updating.Endpoint
import           Utils.Preludes.Server

type ContractorProfileApi
   = "profiles" :> GenericApiAuth "contractors" ContractorProfile ContractorProfileFilter

contractorProfileApp :: ServerT ContractorProfileApi AppM
contractorProfileApp =
  listR SqlStorage :<|> createR SqlStorage :<|> getR SqlStorage :<|>
  updateR SqlStorage :<|>
  deleteR SqlStorage
