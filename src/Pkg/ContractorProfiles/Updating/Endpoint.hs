module Pkg.ContractorProfiles.Updating.Endpoint where

import           Pkg.ContractorProfiles.Updating.Service
import           Utils.Preludes.Endpoint            (ApiResponse (..),
                                                          AppM, AuthProtectData,
                                                          ContractorProfile,
                                                          ContractorProfileId,
                                                          StorageTypes)

updateR ::
     StorageTypes
  -> AuthProtectData
  -> ContractorProfileId
  -> ContractorProfile
  -> AppM (ApiResponse ContractorProfile)
updateR s auth pid reqBody = do
  updatedProfile <- contractorProfilesUpdater s pid reqBody
  case updatedProfile of
    Left err -> return $ ApiResponse err Nothing
    Right _  -> return $ ApiResponse "Successfully updated profile" Nothing
