{-# LANGUAGE FlexibleContexts #-}

module Pkg.ContractorProfiles.Updating.Service where

import           Utils.Preludes.Service          (ConnectionPool,
                                                       ContractorProfile,
                                                       ContractorProfileId, Has,
                                                       MonadBaseControl,
                                                       MonadIO, MonadReader,
                                                       QueryFilter,
                                                       StorageTypes, Text)
import           Storage.ContractorProfiles.Functions

contractorProfilesUpdater ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> ContractorProfileId
  -> ContractorProfile
  -> m (Either Text ())
contractorProfilesUpdater s pid profile = do
  existingProfileOpt <- retrieveContractorProfile s pid
  case existingProfileOpt of
    Nothing -> return $ Left "Profile does not exist"
    Just existingProfile -> do
      updateContractorProfile s pid profile
      return $ Right ()
