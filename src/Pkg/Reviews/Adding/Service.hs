{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE ScopedTypeVariables #-}


module Pkg.Reviews.Adding.Service where

import           Storage.Reviews.Functions
import           Storage.Users.Functions   (retrieveUser)
import           Utils.Preludes.Service    (ConnectionPool, Has,
                                            MonadBaseControl, MonadIO,
                                            MonadReader, Review (..), ReviewId,
                                            StorageTypes, Text, runIfRetrieved)

reviewAdder ::
     forall env m s field.
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> Review
  -> m (Either Text Review)
reviewAdder s review@Review {reviewPoster, reviewReviewee} =
  runIfRetrieved "Poster does not exist" (retrieveUser s reviewPoster) $
  runIfRetrieved "Reviewee does not exist" (retrieveUser s reviewReviewee) $ do
    (_, createdReview) <- saveReview s review
    return $ Right createdReview
