module Pkg.Reviews.Adding.Endpoint where

import           Pkg.Reviews.Adding.Service (reviewAdder)
import           Utils.Preludes.Endpoint    (ApiResponse (..), AppM,
                                             AuthProtectData, Review, ReviewId,
                                             StorageTypes)

createReviewR ::
     StorageTypes -> AuthProtectData -> Review -> AppM (ApiResponse Review)
createReviewR s auth@(userid, u, _t) entity = do
  e <- reviewAdder s entity
  case e of
    Left err -> return $ ApiResponse err Nothing
    Right r  -> return $ ApiResponse "Review created" (Just r)
