module Pkg.Reviews.Removing.Endpoint where

import           Pkg.Reviews.Removing.Service
import           Utils.Preludes.Endpoint (ApiResponse (..), AppM,
                                               AuthProtectData, Review,
                                               ReviewId, StorageTypes)

deleteReviewR ::
     StorageTypes -> AuthProtectData -> ReviewId -> AppM (ApiResponse Review)
deleteReviewR s _auth eid = do
  reviewDeleter s eid
  return $ ApiResponse "Review deleted" Nothing
