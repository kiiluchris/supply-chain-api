{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}


module Pkg.Reviews.Removing.Service where

import           Utils.Preludes.Service (ConnectionPool, Has,
                                              MonadBaseControl, MonadIO,
                                              MonadReader, Review, ReviewId,
                                              StorageTypes, Text)
import           Storage.Reviews.Functions

reviewDeleter ::
     forall env m s field.
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> ReviewId
  -> m ()
reviewDeleter s rid = do
  deleteReview s rid
  return ()
