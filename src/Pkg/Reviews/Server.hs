{-# LANGUAGE DataKinds #-}

module Pkg.Reviews.Server where

import           Servant                         ((:<|>) (..), Proxy (Proxy),
                                                  ServerT)

import           App.AppM                        (AppM)
import           Models                          (Review)
import           Pkg.Reviews.Adding.Endpoint     (createReviewR)
import           Pkg.Reviews.Querying.Endpoint   (listReviewR)
import           Pkg.Reviews.Removing.Endpoint   (deleteReviewR)
import           Pkg.Reviews.Retrieving.Endpoint (getReviewR)
import           Pkg.Reviews.Updating.Endpoint   (updateReviewR)
import           Utils.Routing               (GenericApiAuth)
import           Storage.Filters                 (ReviewFilter)
import           Storage.Types                   (StorageTypes (..))

type ReviewApi = GenericApiAuth "reviews" Review ReviewFilter

reviewApp :: ServerT ReviewApi AppM
reviewApp =
  listReviewR SqlStorage :<|> createReviewR SqlStorage :<|>
  getReviewR SqlStorage :<|>
  updateReviewR SqlStorage :<|>
  deleteReviewR SqlStorage
