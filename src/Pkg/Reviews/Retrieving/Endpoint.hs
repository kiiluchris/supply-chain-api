module Pkg.Reviews.Retrieving.Endpoint where

import           Pkg.Reviews.Retrieving.Service (reviewRetriever)
import           Utils.Preludes.Endpoint   (ApiResponse (..), AppM,
                                                 AuthProtectData, Review,
                                                 ReviewId, StorageTypes,
                                                 throw404)

getReviewR :: StorageTypes -> AuthProtectData -> ReviewId -> AppM Review
getReviewR s auth eid = do
  e <- reviewRetriever s eid
  maybe (throw404 "Review does not exist") return e
