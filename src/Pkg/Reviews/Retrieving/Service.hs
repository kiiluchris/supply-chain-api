{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}


module Pkg.Reviews.Retrieving.Service where

import           Utils.Preludes.Service (ConnectionPool, Has,
                                              MonadBaseControl, MonadIO,
                                              MonadReader, Review, ReviewId,
                                              StorageTypes, Text)
import           Storage.Reviews.Functions

reviewRetriever ::
     forall env m s field.
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> ReviewId
  -> m (Maybe Review)
reviewRetriever s rid = do
  reviewOpt <- retrieveReview s rid
  return $ fmap snd reviewOpt
