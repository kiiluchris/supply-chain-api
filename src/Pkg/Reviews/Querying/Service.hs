{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Pkg.Reviews.Querying.Service where

import           Storage.Reviews.Functions
import           Utils.Preludes.Service    (ConnectionPool, Has,
                                            MonadBaseControl, MonadIO,
                                            MonadReader, QueryFilter, Review,
                                            ReviewFilter, StorageTypes)

reviewQuerant ::
     forall env m s field.
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> Maybe (QueryFilter ReviewFilter)
  -> m [Review]
reviewQuerant = queryReviews
