module Pkg.Reviews.Querying.Endpoint where

import           Pkg.Reviews.Querying.Service (reviewQuerant)
import           Utils.Preludes.Endpoint      (ApiResponse (..), AppM,
                                               AuthProtectData, QueryFilter,
                                               Review, ReviewFilter,
                                               StorageTypes)

listReviewR ::
     StorageTypes
  -> AuthProtectData
  -> Maybe (QueryFilter ReviewFilter)
  -> AppM [Review]
listReviewR s auth = reviewQuerant s
