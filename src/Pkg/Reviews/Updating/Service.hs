{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}


module Pkg.Reviews.Updating.Service where

import           Utils.Preludes.Service (ConnectionPool, Has,
                                              MonadBaseControl, MonadIO,
                                              MonadReader, Review (..),
                                              ReviewId, StorageTypes, Text)
import           Storage.Reviews.Functions

reviewUpdater ::
     ( MonadReader env m
     , MonadIO m
     , MonadBaseControl IO m
     , Has ConnectionPool env
     )
  => StorageTypes
  -> ReviewId
  -> Review
  -> m (Maybe Review)
reviewUpdater s rid review = do
  let Review {reviewComment = comment, reviewRating = rating} = review
  oldReviewOpt <- retrieveReview s rid
  case oldReviewOpt of
    Nothing -> return Nothing
    Just (_, oldReview') -> do
      let updatedReview =
            oldReview' {reviewComment = comment, reviewRating = rating}
      updateReview s rid updatedReview
      return $ Just updatedReview
