module Pkg.Reviews.Updating.Endpoint where

import           Pkg.Reviews.Updating.Service
import           Utils.Preludes.Endpoint (ApiResponse (..), AppM,
                                               AuthProtectData, Review,
                                               ReviewId, StorageTypes)

updateReviewR ::
     StorageTypes
  -> AuthProtectData
  -> ReviewId
  -> Review
  -> AppM (ApiResponse Review)
updateReviewR s _auth eid entity = do
  updatedReview <- reviewUpdater s eid entity
  return $
    case updatedReview of
      Nothing -> ApiResponse "Review could not be found" Nothing
      Just updatedReview' -> ApiResponse "Review updated" (Just updatedReview')
