{-# LANGUAGE FlexibleContexts #-}

module Pkg.UserProjectTasks.Querying.Service where

import           Storage.ProjectTasks.Functions
import           Utils.Preludes.Service         (ConnectionPool, Has,
                                                 MonadBaseControl, MonadIO,
                                                 MonadReader, ProjectId,
                                                 ProjectTask (..),
                                                 ProjectTaskFilter (..),
                                                 ProjectTaskId,
                                                 QueryFilter (..), StorageTypes,
                                                 Text, UserId, defaultFilter)

userProjectTasksQuerant ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> UserId
  -> ProjectId
  -> Maybe (QueryFilter ProjectTaskFilter)
  -> m [ProjectTask]
userProjectTasksQuerant s uid pid filters =
  queryProjectTasks s (setQueryFilterQMaybe uid pid filters)
  where
    setProjectTaskOwner uid p = p {projectTaskFilterOwner = Just uid}
    setProjectTaskProject pid p = p {projectTaskFilterProject = Just pid}
    setProjectTaskOwnerMaybe uid pid Nothing =
      setProjectTaskOwnerMaybe uid pid (Just defaultFilter)
    setProjectTaskOwnerMaybe uid pid p@(Just _) =
      setProjectTaskProject pid . setProjectTaskOwner uid <$> p
    setQueryFilterQ p q = q {queryFilterQ = p}
    setQueryFilterQMaybe uid pid Nothing =
      setQueryFilterQMaybe uid pid (Just defaultFilter)
    setQueryFilterQMaybe uid pid queryFilterOpt@(Just queryFilter) =
      (setQueryFilterQ . setProjectTaskOwnerMaybe uid pid)
        (queryFilterQ queryFilter) <$>
      queryFilterOpt
