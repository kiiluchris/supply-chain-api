module Pkg.UserProjectTasks.Querying.Endpoint where

import           Pkg.UserProjectTasks.Querying.Service
import           Utils.Preludes.Endpoint               (ApiResponse (..), AppM,
                                                        AuthProtectData,
                                                        ProjectId, ProjectTask,
                                                        ProjectTaskFilter,
                                                        ProjectTaskId,
                                                        QueryFilter,
                                                        StorageTypes, UserId,
                                                        throw401, throw404)

listR ::
     StorageTypes
  -> AuthProtectData
  -> UserId
  -> ProjectId
  -> Maybe (QueryFilter ProjectTaskFilter)
  -> AppM [ProjectTask]
listR s auth = userProjectTasksQuerant s
