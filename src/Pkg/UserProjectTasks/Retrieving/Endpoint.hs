module Pkg.UserProjectTasks.Retrieving.Endpoint where

import           Utils.Preludes.Endpoint            (ApiResponse (..),
                                                          AppM, AuthProtectData,
                                                          ProjectId,
                                                          ProjectTask,
                                                          ProjectTaskId,
                                                          StorageTypes, UserId,
                                                          throw401, throw404)
import           Pkg.UserProjectTasks.Retrieving.Service

getR ::
     StorageTypes
  -> AuthProtectData
  -> UserId
  -> ProjectId
  -> ProjectTaskId
  -> AppM ProjectTask
getR s auth uid pid tid = do
  projectTaskOpt <- userProjectTasksRetriever s uid pid tid
  maybe (throw404 "Project Task not found") return projectTaskOpt
