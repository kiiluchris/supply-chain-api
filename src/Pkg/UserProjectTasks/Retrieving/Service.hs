{-# LANGUAGE FlexibleContexts #-}

module Pkg.UserProjectTasks.Retrieving.Service where

import           Utils.Preludes.Service    (ConnectionPool, Has,
                                                 MonadBaseControl, MonadIO,
                                                 MonadReader, ProjectId,
                                                 ProjectTask (..),
                                                 ProjectTaskId, StorageTypes,
                                                 Text, UserId)
import           Storage.ProjectTasks.Functions

userProjectTasksRetriever ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> UserId
  -> ProjectId
  -> ProjectTaskId
  -> m (Maybe ProjectTask)
userProjectTasksRetriever s uid pid tid = do
  projectTaskOpt <- retrieveProjectTask s tid
  return (checkProjectAccessValid uid pid =<< fmap snd projectTaskOpt)
  where
    checkProjectAccessValid uid pid p@ProjectTask { projectTaskOwner = owner
                                                  , projectTaskProject = project
                                                  }
      | owner == uid && project == pid = Just p
      | otherwise = Nothing
