module Pkg.UserProjectTasks.Removing.Endpoint where

import           Utils.Preludes.Endpoint          (ApiResponse (..), AppM,
                                                        AuthProtectData,
                                                        ProjectId, ProjectTask,
                                                        ProjectTaskId,
                                                        StorageTypes, UserId,
                                                        throw401, throw404)
import           Pkg.UserProjectTasks.Removing.Service

deleteR ::
     StorageTypes
  -> AuthProtectData
  -> UserId
  -> ProjectId
  -> ProjectTaskId
  -> AppM (ApiResponse ProjectTask)
deleteR s auth uid pid tid = do
  deletedTask <- userProjectTasksDeleter s uid pid tid
  case deletedTask of
    Left err -> return $ ApiResponse err Nothing
    Right _  -> return $ ApiResponse "Task deleted" Nothing
