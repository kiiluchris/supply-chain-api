{-# LANGUAGE FlexibleContexts #-}

module Pkg.UserProjectTasks.Removing.Service where

import           Utils.Preludes.Service    (ConnectionPool, Has,
                                                 MonadBaseControl, MonadIO,
                                                 MonadReader, ProjectId,
                                                 ProjectTask (..),
                                                 ProjectTaskId, StorageTypes,
                                                 Text, UserId)
import           Storage.ProjectTasks.Functions

userProjectTasksDeleter ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> UserId
  -> ProjectId
  -> ProjectTaskId
  -> m (Either Text ())
userProjectTasksDeleter s uid pid tid = do
  existingTaskOpt <- retrieveProjectTask s tid
  case existingTaskOpt of
    Just (_, ProjectTask { projectTaskOwner = owner
                         , projectTaskProject = project
                         })
      | owner == uid && project == pid -> return $ Right ()
    _ -> return $ Left "Project does not exist"
