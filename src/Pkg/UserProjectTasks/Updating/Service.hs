{-# LANGUAGE FlexibleContexts #-}

module Pkg.UserProjectTasks.Updating.Service where

import           Utils.Preludes.Service    (ConnectionPool, Has,
                                                 MonadBaseControl, MonadIO,
                                                 MonadReader, ProjectId,
                                                 ProjectTask (..),
                                                 ProjectTaskId, StorageTypes,
                                                 Text, UserId)
import           Storage.ProjectTasks.Functions

userProjectTasksUpdater ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> UserId
  -> ProjectId
  -> ProjectTaskId
  -> ProjectTask
  -> m (Either Text ())
userProjectTasksUpdater s uid pid tid updateData = do
  existingProjectTask <- retrieveProjectTask s tid
  runIfProjectExists existingProjectTask $ \p -> do
    updateProjectTask s tid updateData
    return $ Right ()
  where
    runIfProjectExists (Just (_, p@ProjectTask { projectTaskOwner = owner
                                               , projectTaskProject = project
                                               })) action
      | owner == uid && project == pid = action p
    runIfProjectExists _ _ = return $ Left "Project task does not exist"
