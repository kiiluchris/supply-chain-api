module Pkg.UserProjectTasks.Updating.Endpoint where

import           Utils.Preludes.Endpoint          (ApiResponse (..), AppM,
                                                        AuthProtectData,
                                                        ProjectId, ProjectTask,
                                                        ProjectTaskId,
                                                        StorageTypes, UserId,
                                                        throw401, throw404)
import           Pkg.UserProjectTasks.Updating.Service

updateR ::
     StorageTypes
  -> AuthProtectData
  -> UserId
  -> ProjectId
  -> ProjectTaskId
  -> ProjectTask
  -> AppM (ApiResponse ProjectTask)
updateR s auth uid pid tid reqBody = do
  value <- userProjectTasksUpdater s uid pid tid reqBody
  return $ ApiResponse "Unimplemented" Nothing
