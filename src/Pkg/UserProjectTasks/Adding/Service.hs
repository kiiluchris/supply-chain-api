{-# LANGUAGE FlexibleContexts #-}

module Pkg.UserProjectTasks.Adding.Service where

import           Storage.Projects.Functions     (retrieveProject)
import           Storage.ProjectTasks.Functions
import           Storage.Users.Functions        (retrieveUser)
import           Utils.Preludes.Service         (ConnectionPool, Has,
                                                 MonadBaseControl, MonadIO,
                                                 MonadReader, ProjectId,
                                                 ProjectTask (..),
                                                 ProjectTaskId, StorageTypes,
                                                 Text, UserId, runIfRetrieved)

userProjectTasksCreator ::
     ( MonadReader env m
     , Has ConnectionPool env
     , MonadIO m
     , MonadBaseControl IO m
     )
  => StorageTypes
  -> UserId
  -> ProjectId
  -> ProjectTask
  -> m (Either Text ProjectTask)
userProjectTasksCreator s uid pid projectTask = do
  let p = projectTask {projectTaskOwner = uid, projectTaskProject = pid}
  runIfRetrieved "Project does not exist" (retrieveProject s pid) $
    runIfRetrieved "User does not exist" (retrieveUser s uid) $ do
      (_, createdProject) <- saveProjectTask s p
      return $ Right createdProject
