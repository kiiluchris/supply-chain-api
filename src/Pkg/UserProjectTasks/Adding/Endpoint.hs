module Pkg.UserProjectTasks.Adding.Endpoint where

import           Pkg.UserProjectTasks.Adding.Service
import           Utils.Preludes.Endpoint             (ApiResponse (..), AppM,
                                                      AuthProtectData,
                                                      ProjectId, ProjectTask,
                                                      ProjectTaskId,
                                                      StorageTypes, UserId,
                                                      throw401, throw404)

createR ::
     StorageTypes
  -> AuthProtectData
  -> UserId
  -> ProjectId
  -> ProjectTask
  -> AppM (ApiResponse ProjectTask)
createR s auth uid pid reqBody = do
  createdTask <- userProjectTasksCreator s uid pid reqBody
  case createdTask of
    Left err -> return $ ApiResponse err Nothing
    Right t  -> return $ ApiResponse "Successfully created task" (Just t)
