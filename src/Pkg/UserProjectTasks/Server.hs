{-# LANGUAGE DataKinds #-}

module Pkg.UserProjectTasks.Server where

import           Utils.Preludes.Server
import           Pkg.UserProjectTasks.Adding.Endpoint
import           Pkg.UserProjectTasks.Querying.Endpoint
import           Pkg.UserProjectTasks.Removing.Endpoint
import           Pkg.UserProjectTasks.Retrieving.Endpoint
import           Pkg.UserProjectTasks.Updating.Endpoint

type UserProjectTaskApi
   = GenericApiNestedAuth' "users" UserId "projects" ProjectId "tasks" ProjectTask ProjectTaskFilter

userProjectTaskApp :: ServerT UserProjectTaskApi AppM
userProjectTaskApp auth uid pid =
  listR SqlStorage auth uid pid :<|> createR SqlStorage auth uid pid :<|>
  getR SqlStorage auth uid pid :<|>
  updateR SqlStorage auth uid pid :<|>
  deleteR SqlStorage auth uid pid
