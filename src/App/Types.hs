{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module App.Types where

import           Data.Aeson
import           Data.ByteString.Lazy as LBS (fromStrict)
import           Data.Foldable        (asum)
import           Data.Swagger         (ToSchema)
import           Data.Text            (Text, pack)
import           Data.Text.Encoding   (encodeUtf8)
import           GHC.Generics         (Generic)
import           Servant.API          (FromHttpApiData (..))

import           Models

data AuthRequest =
  AuthRequest
    { authRequestName     :: Maybe Text
    , authRequestPassword :: Maybe Text
    , authRequestToken    :: Maybe Text
    }
  deriving (Show, Generic)

data ApiResponse a =
  ApiResponse
    { apiResMessage :: Text
    , apiResData    :: Maybe a
    }
  deriving (Show, Generic)

type AuthResponse = ApiResponse ApiToken

newtype ApiToken =
  ApiToken Text
  deriving (Generic, Show)

data UserRegister =
  UserRegister
    { userRegName        :: Text
    , userRegUserName    :: Text
    , userRegPassword    :: Text
    , userRegPhoneNumber :: Text
    }
  deriving (Generic, Show)

type Token = Text

type AuthProtectData = (UserId, User, Token)

instance FromJSON ApiToken where
  parseJSON = withObject "ApiToken" $ \p -> ApiToken <$> p .: "token"

instance ToJSON ApiToken where
  toJSON (ApiToken token) = object ["token" .= token]
  toEncoding (ApiToken token) = pairs ("token" .= token)

instance FromJSON AuthRequest where
  parseJSON =
    withObject "AuthRequest" $ \p ->
      AuthRequest <$> p .:? "name" <*> p .:? "password" <*> p .:? "token"

instance ToJSON AuthRequest where
  toJSON (AuthRequest name pswd token) =
    object ["name" .= name, "password" .= pswd, "token" .= token]
  toEncoding (AuthRequest name pswd token) =
    pairs ("name" .= name <> "password" .= pswd <> "token" .= token)

instance FromJSON UserRegister where
  parseJSON =
    withObject "UserRegister" $ \p ->
      UserRegister <$> p .: "name" <*> p .: "userName" <*> p .: "password" <*>
      p .: "phoneNumber"

instance ToJSON UserRegister where
  toJSON (UserRegister name user_name pswd phone) =
    object
      [ "name" .= name
      , "userName" .= user_name
      , "password" .= pswd
      , "phoneNumber" .= phone
      ]
  toEncoding (UserRegister name user_name pswd phone) =
    pairs
      ("name" .= name <>
       "userName" .= user_name <> "password" .= pswd <> "phoneNumber" .= phone)

instance (ToJSON a) => ToJSON (ApiResponse a) where
  toJSON (ApiResponse msg data_) = object ["message" .= msg, "data" .= data_]
  toEncoding (ApiResponse msg data_) =
    pairs ("message" .= msg <> "data" .= data_)
