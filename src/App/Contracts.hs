{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}

module App.Contracts where

import           Control.Monad.Reader (MonadReader, asks)
import           Database.Persist.Sql (ConnectionPool)
import           Web.JWT              (Signer)

import           App.Env.Data         (AppEnv (..))
import           App.Types            (AuthProtectData)

class Has field env where
  retrieve :: env -> field

instance Has ConnectionPool AppEnv where
  retrieve = envDbPool

instance Has (Maybe AuthProtectData) AppEnv where
  retrieve = envAuthData

instance Has Signer AppEnv where
  retrieve = envJwtSecret

grab ::
     forall field env m. (MonadReader env m, Has field env)
  => m field
grab = asks $ retrieve @field
