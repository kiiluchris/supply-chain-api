module App.Env.Data where

import           Database.Persist.Sql (ConnectionPool)
import           Web.JWT              (Signer)

import           App.Types            (AuthProtectData)

data AppEnv =
  AppEnv
    { envDbPool    :: !ConnectionPool
    , envAuthData  :: !(Maybe AuthProtectData)
    , envJwtSecret :: !Signer
    }
