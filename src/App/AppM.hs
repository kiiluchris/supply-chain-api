{-# LANGUAGE DataKinds        #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}

module App.AppM
  ( AppM
  , AppM'
  , hoistAppMServer
  , hoistAppMServerWithCtx
  , hoistTransformer'
  ) where

import           Control.Monad.Trans.Reader (ReaderT, runReaderT)
import           Servant                    (Context, Handler, HasServer,
                                             Proxy (Proxy), Server, ServerT,
                                             hoistServer,
                                             hoistServerWithContext)

import           App.Env                    (AppEnv)
import           App.Types                  (AuthProtectData)

type Env = AppEnv

type AppM = ReaderT Env Handler

type AppM' = ReaderT AuthProtectData AppM

hoistTransformer :: forall api x. Env -> AppM x -> Handler x
hoistTransformer env handler = runReaderT handler env

hoistTransformer' :: forall api x. AuthProtectData -> AppM' x -> AppM x
hoistTransformer' auth handler = runReaderT handler auth

hoistAppMServer ::
     forall api ctx. (HasServer api '[])
  => Env
  -> Proxy api
  -> ServerT api AppM
  -> ServerT api Handler
hoistAppMServer env proxy = hoistServer proxy (hoistTransformer env)

hoistAppMServerWithCtx ::
     forall api (ctx :: [*]). (HasServer api ctx)
  => Env
  -> Proxy api
  -> Proxy ctx
  -> ServerT api AppM
  -> ServerT api Handler
hoistAppMServerWithCtx env serverProxy contextProxy =
  hoistServerWithContext serverProxy contextProxy (hoistTransformer env)
