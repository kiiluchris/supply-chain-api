{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module App.Env
  ( module App.Env.Data
  , defaultEnv
  ) where

import           Database.Persist.Sql (ConnectionPool)

import           App.Env.Data
import           App.Types            (AuthProtectData)
import           Effects.JwtAuth      (genJwtSecret)

defaultEnv :: ConnectionPool -> Maybe AuthProtectData -> IO AppEnv
defaultEnv envDbPool envAuthData = do
  envJwtSecret <- genJwtSecret
  return $ AppEnv {..}
