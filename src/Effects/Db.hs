{-# LANGUAGE ConstraintKinds   #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeApplications  #-}

module Effects.Db
  ( WithDb
  , withPool
  , runWithPool
  ) where

import           Conduit                    (ResourceT)
import           Control.Monad              (liftM)
import           Control.Monad.IO.Class     (MonadIO, liftIO)
import           Control.Monad.IO.Unlift    (MonadUnliftIO, unliftIO)
import           Control.Monad.Logger       (NoLoggingT)
import           Control.Monad.Reader       (MonadReader)
import           Control.Monad.Trans.Reader (ReaderT)
import           Database.Persist.Sql       (ConnectionPool, SqlBackend,
                                             liftSqlPersistMPool,
                                             runSqlPersistMPool, runSqlPool)

import           App.Contracts              (Has, grab)

type WithDb env m = (MonadReader env m, Has ConnectionPool env)

type DbReaderT record = ReaderT SqlBackend (NoLoggingT (ResourceT IO)) record

runWithPool :: (WithDb env m, MonadIO m) => ConnectionPool -> DbReaderT a -> m a
runWithPool = flip liftSqlPersistMPool

withPool :: (WithDb env m, MonadIO m) => DbReaderT a -> m a
withPool query = do
  pool <- grab @ConnectionPool
  runWithPool pool query
