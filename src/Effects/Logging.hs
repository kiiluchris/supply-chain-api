{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE FlexibleContexts   #-}
{-# LANGUAGE RecordWildCards    #-}
{-# LANGUAGE StandaloneDeriving #-}

module Effects.Logging
  ( Effects.Logging.logError
  , Effects.Logging.logInfo
  , Effects.Logging.logDebug
  ) where

import           Control.Monad.IO.Class      (MonadIO, liftIO)
import           Control.Monad.Logger
import           Control.Monad.Reader        (MonadReader)
import           Control.Monad.Trans.Control (MonadBaseControl)
import           Data.Aeson                  (FromJSON, ToJSON (..),
                                              defaultOptions, encode,
                                              fieldLabelModifier,
                                              genericToEncoding, genericToJSON,
                                              toEncoding)
import qualified Data.ByteString.Lazy.Char8  as C (unpack)
import           Data.Char                   (toLower)
import           Data.List                   (stripPrefix)
import           Data.Text                   (Text, pack)
import           Data.Time                   (UTCTime, getCurrentTime)
import           GHC.Generics                (Generic)
import           System.IO                   (FilePath)

import           App.Contracts               (Has, grab)
import           Utils.Json                  (genericJsonOptions)

deriving instance Generic LogLevel

instance FromJSON LogLevel

instance ToJSON LogLevel

data LogMessage =
  LogMessage
    { logMessage   :: Text
    , logLevel     :: LogLevel
    , logTimestamp :: UTCTime
    , logSrcFile   :: Text
    }
  deriving (Show, Generic)

instance FromJSON LogMessage

instance ToJSON LogMessage where
  toEncoding = genericToEncoding $ genericJsonOptions "log"

instance ToLogStr LogMessage where
  toLogStr = toLogStr . encode

logFilePath :: LogLevel -> FilePath
logFilePath LevelDebug = "debug.log"
logFilePath LevelError = "error.log"
logFilePath LevelInfo  = "info.log"
logFilePath _          = "random.log"

mkLogMessage :: (MonadIO m) => LogLevel -> Text -> Text -> m LogMessage
mkLogMessage logLevel logSrcFile logMessage = do
  logTimestamp <- liftIO getCurrentTime
  return LogMessage {..}

logError :: (MonadBaseControl IO m, MonadIO m) => Text -> Text -> m ()
logError = printLog LevelError

logInfo :: (MonadBaseControl IO m, MonadIO m) => Text -> Text -> m ()
logInfo = printLog LevelInfo

logDebug :: (MonadBaseControl IO m, MonadIO m) => Text -> Text -> m ()
logDebug = printLog LevelDebug

printLog ::
     (MonadBaseControl IO m, MonadIO m) => LogLevel -> Text -> Text -> m ()
printLog lvl src msg = do
  logMsg <- pack . C.unpack . encode <$> mkLogMessage lvl src msg
  runFileLoggingT (logFilePath lvl) (logOtherNS src lvl logMsg)
