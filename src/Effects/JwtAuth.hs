{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeApplications      #-}

module Effects.JwtAuth
  ( Signer
  , genJwtSecret
  , mkToken
  , decodeToken
  , decodeTokenAppM
  , retrieveTokenData
  ) where

import           Control.Monad.IO.Class (MonadIO)
import           Control.Monad.Reader   (MonadReader)
import           Data.Aeson             (Value (String))
import qualified Data.Map               as Map (Map, fromList)
import           Data.Text              (Text, pack)
import qualified Data.UUID              as UUID (toString)
import qualified Data.UUID.V4           as UUID (nextRandom)
import           Web.JWT                (ClaimsMap (..), JWT, JWTClaimsSet (..),
                                         Signer, VerifiedJWT, claims,
                                         decodeAndVerifySignature, encodeSigned,
                                         hmacSecret, stringOrURI)

import           App.Contracts          (Has, grab)
import           App.Types              (AuthProtectData)

genJwtSecret :: IO Signer
genJwtSecret = hmacSecret . pack . UUID.toString <$> UUID.nextRandom

mkToken :: (Has Signer env, MonadReader env m) => Text -> m Text
mkToken userName = do
  jwtSignature <- grab @Signer
  return $ encodeSigned jwtSignature mempty claims
  where
    claims =
      mempty
        { unregisteredClaims =
            ClaimsMap $ Map.fromList [("userName", String userName)]
        }

decodeToken :: Signer -> Text -> Maybe (JWT VerifiedJWT)
decodeToken = decodeAndVerifySignature

decodeTokenAppM ::
     (Has Signer env, MonadReader env m) => Text -> m (Maybe (JWT VerifiedJWT))
decodeTokenAppM token = flip decodeToken token <$> grab @Signer

retrieveTokenData :: JWT VerifiedJWT -> Map.Map Text Value
retrieveTokenData = (\(ClaimsMap c) -> c) . unregisteredClaims . claims
