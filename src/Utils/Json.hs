module Utils.Json
  ( genericJsonOptions
  , genericJsonOptionsKebab
  , stripPrefix
  ) where

import           Data.Aeson  (Options (fieldLabelModifier), camelTo2,
                              defaultOptions)
import qualified Data.Char   as Char (toLower, toUpper)
import qualified Data.List   as List (stripPrefix)
import           Text.Casing (fromHumps, fromKebab, toKebab, toPascal)

stripPrefix :: String -> String -> String
stripPrefix prefix propertyName =
  case List.stripPrefix prefix propertyName of
    Nothing     -> propertyName
    Just []     -> propertyName
    Just (p:ps) -> Char.toLower p : ps

genericJsonOptions :: String -> Options
genericJsonOptions prefix =
  defaultOptions {fieldLabelModifier = stripPrefix prefix}

genericJsonOptionsKebab :: String -> Char -> Options
genericJsonOptionsKebab prefix delim =
  defaultOptions {fieldLabelModifier = toKebab . fromHumps . stripPrefix prefix}
