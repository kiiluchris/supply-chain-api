module Utils.Password where

import           Control.Monad.IO.Class (MonadIO, liftIO)
import           Crypto.BCrypt
import qualified Data.ByteString.Char8  as C (ByteString, pack, unpack)
import qualified Data.Text              as T (Text, pack, unpack)

import           Models                 (User (..))

text2ByteString = C.pack . T.unpack

byteString2Text = T.pack . C.unpack

verifyUserPassword :: User -> T.Text -> Bool
verifyUserPassword = verifyPassword . userPassword

verifyPassword :: T.Text -> T.Text -> Bool
verifyPassword userPswd pswd =
  validatePassword (text2ByteString userPswd) (text2ByteString pswd)

-- passwordHashingPolicy = slowerBcryptHashingPolicy
passwordHashingPolicy = fastBcryptHashingPolicy

hashingFunction = hashPasswordUsingPolicy passwordHashingPolicy

hashPswd :: MonadIO m => T.Text -> m (Maybe T.Text)
hashPswd plaintxt = do
  hashedPswd <- liftIO . hashingFunction . text2ByteString $ plaintxt
  return $ byteString2Text <$> hashedPswd
