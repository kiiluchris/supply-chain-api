module Utils.Preludes.Endpoint
  ( ApiResponse(..)
  , AuthProtectData(..)
  , StorageType(..)
  , StorageTypes(..)
  , AppM
  , Has
  , MonadIO
  , MonadReader
  , grab
  , module Models
  , module Storage.Filters
  , module Effects.Logging
  , throw404
  , throw401
  , liftIO
  ) where

import           Control.Monad.Error.Class (MonadError)
import           Control.Monad.IO.Class    (MonadIO, liftIO)
import           Control.Monad.Reader      (MonadReader)
import           Data.ByteString.Lazy      (ByteString)
import           Servant                   (ServerError, err401, err404,
                                            errBody, throwError)

import           App.Types                 (ApiResponse (..),
                                            AuthProtectData (..))
import           App.AppM                  (AppM)
import           App.Contracts             (Has, grab)
import           Effects.Logging
import           Models
import           Storage.Filters
import           Storage.Types             (StorageType (..), StorageTypes (..))

throwErr :: (MonadError e m, e ~ ServerError) => e -> ByteString -> m a
throwErr err body = throwError err {errBody = body}

throw404 :: (MonadError e m, e ~ ServerError) => ByteString -> m a
throw404 = throwErr err404

throw401 :: (MonadError e m, e ~ ServerError) => ByteString -> m a
throw401 = throwErr err401
