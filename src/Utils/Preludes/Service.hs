module Utils.Preludes.Service
  ( MonadIO
  , MonadReader
  , ConnectionPool
  , Has
  , grab
  , liftIO
  , StorageType(..)
  , StorageTypes(..)
  , Text
  , MonadBaseControl
  , module Models
  , module Storage.Filters
  , module Effects.Logging
  , runIfRetrieved
  , DefaultQueryFilter(..)
  ) where

import           Control.Monad.IO.Class      (MonadIO, liftIO)
import           Control.Monad.Reader        (MonadReader)
import           Control.Monad.Trans.Control (MonadBaseControl)
import           Data.Text                   (Text)
import           Database.Persist.Sql        (ConnectionPool)

import           App.Contracts               (Has, grab)
import           Effects.Logging
import           Models
import           Storage.Filters
import           Storage.Filters.Defaults    (DefaultQueryFilter (..))
import           Storage.Types               (StorageType (..),
                                              StorageTypes (..))

runIfRetrieved ::
     (MonadIO m) => err -> m (Maybe a) -> m (Either err b) -> m (Either err b)
runIfRetrieved err action next = do
  value <- action
  case value of
    Nothing -> return $ Left err
    Just _  -> next
