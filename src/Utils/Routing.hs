{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Utils.Routing where

import           Database.Persist                 (Key)
import           GHC.TypeLits
import           Servant                          ((:<|>), (:>), AuthProtect,
                                                   Capture, Delete, Get, JSON,
                                                   Post, Put, QueryParam,
                                                   ReqBody)
import           Servant.Server.Experimental.Auth (AuthServerData)

import           App.Types                        (ApiResponse, AuthProtectData)
import           Storage.Filters                  (QueryFilter)

type CaptureId i = Capture "id" i

type ListApi a filter = QueryParam "q" (QueryFilter filter) :> Get '[ JSON] [a]

type ListApiAuth a filter = AuthenticatedApi (ListApi a filter)

type CreateApi a = ReqBody '[ JSON] a :> Post '[ JSON] (ApiResponse a)

type CreateApiAuth a = AuthenticatedApi (CreateApi a)

type RetrieveApi i a = CaptureId i :> Get '[ JSON] a

type RetrieveApiAuth i a = AuthenticatedApi (RetrieveApi i a)

type UpdateApi i a
   = CaptureId i :> ReqBody '[ JSON] a :> Put '[ JSON] (ApiResponse a)

type UpdateApiAuth i a = AuthenticatedApi (UpdateApi i a)

type DeleteApi i a = CaptureId i :> Delete '[ JSON] (ApiResponse a)

type DeleteApiAuth i a = AuthenticatedApi (DeleteApi i a)

type BaseApiRoot a i filter
   = (ListApi a filter :<|> CreateApi a :<|> RetrieveApi i a :<|> UpdateApi i a :<|> DeleteApi i a)

type BaseApiRootAuth a i filter
   = (ListApiAuth a filter :<|> CreateApiAuth a :<|> RetrieveApiAuth i a :<|> UpdateApiAuth i a :<|> DeleteApiAuth i a)

type BaseApi (root :: Symbol) a i filter = root :> BaseApiRoot a i filter

type BaseApiAuth (root :: Symbol) a i filter
   = root :> BaseApiRootAuth a i filter

type GenericApi (route :: Symbol) a filter = BaseApi route a (Key a) filter

type GenericApiAuth (route :: Symbol) a filter
   = BaseApiAuth route a (Key a) filter

type GenericApiRootAuth a filter = BaseApiRootAuth a (Key a) filter

type JwtAuthProtect = AuthProtect "JWT"

type instance AuthServerData JwtAuthProtect = AuthProtectData

type AuthenticatedApi api = JwtAuthProtect :> api

type GenericApiNested (route :: Symbol) i (route' :: Symbol) a filter
   = route :> CaptureId i :> GenericApi route' a filter

type GenericApiNestedAuth (route :: Symbol) i (route' :: Symbol) a filter
   = JwtAuthProtect :> GenericApiNested route i route' a filter

type GenericApiNested' (route :: Symbol) i (route' :: Symbol) i' (route'' :: Symbol) a filter
   = route :> CaptureId i :> GenericApiNested route' i' route'' a filter

type GenericApiNestedAuth' (route :: Symbol) i (route' :: Symbol) i' (route'' :: Symbol) a filter
   = JwtAuthProtect :> GenericApiNested' route i route' i' route'' a filter
