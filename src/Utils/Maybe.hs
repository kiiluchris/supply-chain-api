module Utils.Maybe where

maybeToEither e = maybe (Left e) Right

alternativeToEither action option =
  case option of
    Just opt -> Left $ action opt
    Nothing  -> Right ()
