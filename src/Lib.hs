{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}

module Lib
  ( startApp
  , app
  ) where

import           Control.Monad.IO.Class           (liftIO)
import           Control.Monad.Logger             (runStderrLoggingT)
import           Control.Monad.Trans.Reader       (runReaderT)
import           Data.Aeson                       (Value (String))
import qualified Data.ByteString.Char8            as C (ByteString, pack,
                                                        stripPrefix, unpack)
import qualified Data.ByteString.Lazy.Char8       as CL (pack)
import qualified Data.Map                         as Map (lookup)
import           Data.Maybe                       (fromMaybe)
import           Data.String.Conversions          (cs)
import qualified Data.Text                        as T (pack)
import           Database.Esqueleto
import           Database.Persist.Sqlite          (ConnectionPool,
                                                   createSqlitePool, entityKey,
                                                   entityVal, runMigration,
                                                   runSqlPool)
import           GHC.Generics                     (Generic)
import           Network.HTTP.Types               (ok200)
import           Network.Wai
import           Network.Wai.Handler.Warp
import           Servant
import           Servant.Server.Experimental.Auth (AuthHandler, mkAuthHandler)
import           System.Environment               (lookupEnv)
import           System.FilePath                  (FilePath)
import           Web.JWT                          (Signer)

import           App.AppM                         (AppM)
import           App.Contracts                    (grab)
import           App.Env                          (AppEnv, defaultEnv)
import           App.Types                        (AuthProtectData)
import           Docs.Swagger
import           Effects.Db                       (withPool)
import           Effects.JwtAuth                  (decodeToken,
                                                   retrieveTokenData)
import           Models                           (EntityField (UserUserName),
                                                   migrateAll)
import           Pkg.Server                       (RouteApi, mainAppServer)
import           Utils.Maybe                      (maybeToEither)
import           Utils.Preludes.Endpoint          (throw401)

authHandler :: AppEnv -> AuthHandler Request AuthProtectData
authHandler env = mkAuthHandler (flip runReaderT env . authHandler')
  where
    authHandler' :: Request -> AppM AuthProtectData
    authHandler' req = do
      jwtSignature <- grab @Signer
      liftIO $
        print $ do
          authHeader <- lookup "Authorization" $ requestHeaders req
          byteToken <- C.stripPrefix "Bearer " authHeader
          decodeToken jwtSignature . T.pack . C.unpack $ byteToken
      either throw401 verifyUserExists $ do
        token <-
          maybeToEither "Failure: Missing auth header" $ do
            authHeader <- lookup "Authorization" $ requestHeaders req
            byteToken <- C.stripPrefix "Bearer " authHeader
            Just . T.pack . C.unpack $ byteToken
        verifiedToken <-
          maybeToEither "Failure: Bad token" $ decodeToken jwtSignature token
        username <- tokenUsername verifiedToken
        Right (username, token)
    extractAesonUsername (Just (String u)) = Right u
    extractAesonUsername _ = Left "Failure: Token has no username"
    tokenUsername =
      extractAesonUsername . Map.lookup "userName" . retrieveTokenData
    verifyUserExists (username, token) = do
      user <-
        withPool $
        select . from $ \u -> do
          where_ (u ^. UserUserName ==. val username)
          limit 1
          return u
      case user of
        (u:_) -> return (entityKey u, entityVal u, token)
        []    -> throw401 "Failure: Token user does not exist"

genAuthServerContext ::
     AppEnv -> Context (AuthHandler Request AuthProtectData ': '[])
genAuthServerContext env = authHandler env :. EmptyContext

type API = RouteApi :<|> SwaggerSchemaUI "docs" "swagger.json"

server :: AppEnv -> Server API
server env =
  mainAppServer env contextProxy :<|>
  swaggerSchemaUIServer (toSwagger @RouteApi Proxy)
  where
    contextProxy = Proxy :: Proxy (AuthHandler Request AuthProtectData ': '[])

app :: AppEnv -> Application
app env = serveWithContext (Proxy @API) (genAuthServerContext env) (server env)

startApp :: IO ()
startApp = do
  pool <- runStderrLoggingT $ createSqlitePool (cs ("sqlite.db" :: FilePath)) 5
  flip runSqlPool pool $ runMigration migrateAll
  env <- defaultEnv pool Nothing
  envPort <- lookupEnv "PORT"
  let port = read (fromMaybe "8080" envPort) :: Int
  putStrLn $ "Running server on port :" <> show port
  run port $ app env
